os = require 'os'
fs = require 'fs'
path = require 'path'

Rehab = null
UglifyJS = null
coffee = null
coffeelint = null

DEBUG = false
LINT_SYNTAX_ONLY = false
PH_SRC_DIR = "./src"
PH_OUTPUT_DIR = "./js"
PH_TARGET_NAME = "perihelion"

# Note: even if all the linter options are set to 'ignore',
# coffeelint will still catch some (but not all) CoffeeScript
# syntax errors that would otherwise fail on compile.
COFFEELINT_OPTIONS = {
    "no_tabs" : {
        "level" : "warn"
    },

    "no_trailing_whitespace" : {
        "level" : "ignore"
    },

    "max_line_length" : {
        "value": 80,
        "level" : "ignore"
    },

    "camel_case_classes" : {
        "level" : "warn"
    },

    "indentation" : {
        "value" : 4,
        "level" : "warn"
    },

    "no_implicit_braces" : {
        "level" : "error"
    },

    "no_trailing_semicolons" : {
        "level" : "error"
    },

    "no_plusplus" : {
        "level" : "warn"
    },

    "no_throwing_strings" : {
        "level" : "error"
    },

    "cyclomatic_complexity" : {
        "value" : 11,
        "level" : "ignore"
    },

    "no_backticks": {
        "level": "ignore"
    },

    "line_endings": {
        "level": "warn",
        "value": "unix"
    },

    "no_implicit_parens": {
        "level": "ignore"
    },

    "empty_constructor_needs_parens": {
        "level": "ignore"
    },

    "non_empty_constructor_needs_parens": {
        "level": "ignore"
    },

    "no_empty_param_list": {
        "level": "ignore"
    },

    "space_operators": {
        "level": "ignore"
    },

    "duplicate_key": {
        "level": "error"
    },

    "newlines_after_classes": {
        "value": 2,
        "level": "ignore"
    },

    "no_stand_alone_at": {
        "level": "warn"
    },

    "arrow_spacing": {
        "level": "ignore"
    },

    "coffeescript_error": {
        "level": "error"
    }
}

option "-r", "--release", "build in production mode (default)"
option "-d", "--debug", "build in development mode"
option "-m", "--minify", "minify output .js in release mode"
option "-l", "--lint", "run lint on source files before compilation"
option "-s", "--lint-syntax", "run lint to catch syntax errors only, not style issues"

parse_options = (options) ->
    if 'debug' of options
        DEBUG = true

    # Required packages.
    try
        Rehab = require 'rehab'
        coffee = require 'coffee-script'
    catch e
        throw new Error "coffee-script and rehab are required to build"

    # Import appropriate packages depending on option flags.

    if ('lint' of options) or ('lint-syntax' of options)
        try
            coffeelint = require 'coffeelint'

            # Set all coffeelinit options to ignore.
            if 'lint-syntax' of options
                LINT_SYNTAX_ONLY = true
                for lint_opt_name, lint_opt_data of COFFEELINT_OPTIONS
                    lint_opt_data.level = "ignore"

        catch e
            throw new Error "Install coffeelint to the project directory to enable linting."

    if 'minify' of options
        try
            UglifyJS = require 'uglify-js2'
        catch e
            throw new Error "Install uglify-js2 to the project directory enable minification."


task "build", "Build Perihelion from sources.", (options) ->
    try
        parse_options options
    catch e
        console.log e.message + "\n"
        exit 1
        return

    if DEBUG
        console.log "Building '#{PH_TARGET_NAME}' (Development)"
    else
        console.log "Building '#{PH_TARGET_NAME}' (Production)"

    rehab = new Rehab()
    cs_out_file = PH_TARGET_NAME + ".coffee"
    js_out_file = PH_TARGET_NAME + ".js"
    map_out_file = PH_TARGET_NAME + ".map"
    src_contents = ""
    js_output = ""
    sm_output = ""

    # Load all the CoffeeScript files.
    console.log "reading sources:"
    src_paths = rehab.process PH_SRC_DIR
    src_contents_arr = src_paths.map (file_path) ->
        console.log "  #{file_path}"
        return fs.readFileSync(file_path, 'utf-8')

    # Lint/syntax-check sources.
    if coffeelint?
        console.log "linting:"
        lint_warns = 0
        lint_errs = 0
        cs_errs = 0

        for index in [0...src_paths.length]
            # TODO: Remove only the PH_SRC_DIR head of the src_file
            src_file = path.basename src_paths[index]

            for result in coffeelint.lint src_contents_arr[index], COFFEELINT_OPTIONS
                msg = "  #{src_file}(#{result.lineNumber}):"

                if result.rule == "coffeescript_error"
                    cs_errs += 1
                else if result.level == "warn"
                    msg += "LintWarn:"
                    lint_warns += 1
                else if result.level == "error"
                    msg += "LintErr:"
                    lint_errs += 1

                msg += "#{result.message}. "
                msg += "#{result.context}." if result.context
                console.log msg

        finish_msg = "  #{cs_errs} error(s)"
        if not LINT_SYNTAX_ONLY
            finish_msg = finish_msg + ", #{lint_errs} style error(s), #{lint_warns} style warning(s)."

        console.log finish_msg

        if lint_errs > 0 or cs_errs > 0
            exit 1
            return

    # Concatenate all CoffeeScript files into a single source,
    # equivalent behavior to the CLI compiler's --join option.
    src_contents = src_contents_arr.join "\n\n"

    # Prepend the debug flag code.
    # http://jstarrdewar.com/blog/2013/02/28/use-uglify-to-automatically-strip-debug-messages-from-your-javascript
    if DEBUG
        src_contents = "`if (typeof DEBUG === 'undefined') DEBUG = true;`\n" + src_contents
    else
        src_contents = "`if (typeof DEBUG === 'undefined') DEBUG = false;`\n" + src_contents

    # Compile.
    # Dev build will generate source maps for live debugging.
    console.log "compiling..."
    if DEBUG
        compiled = coffee.compile src_contents, {
            sourceMap: true,
            generatedFile: js_out_file,
            sourceFiles: [cs_out_file]
        }

        js_output = compiled.js
        sm_output = compiled.v3SourceMap
    else
        js_output = coffee.compile src_contents

    # Minification.
    if UglifyJS?
        if DEBUG
            # Feed CoffeeScript's generated source map into the
            # UglifyJS OutputStream so that CS mappings will be
            # preserved after compression.
            source_map = UglifyJS.SourceMap {orig: sm_output}
            stream = UglifyJS.OutputStream {source_map: source_map}
            compressor = UglifyJS.Compressor()
            ast = UglifyJS.parse js_output

            ast.figure_out_scope()
            ast = ast.transform compressor
            ast.print(stream)

            js_output = stream.toString()
            sm_output = source_map.toString()
        else
            # Strip debug conditional blocks in release code.
            compressor = UglifyJS.Compressor {global_defs: {DEBUG:false}}
            ast = UglifyJS.parse js_output

            ast.figure_out_scope()
            ast = ast.transform compressor

            js_output = ast.print_to_string()


    # Write 'em all out. (Note that the concatenated CoffeeScript source needs to be
    # emitted as well in debug mode for the source map to work.)

    try
        fs.mkdirSync PH_OUTPUT_DIR
    catch e
        throw e if e.code != 'EEXIST'

    if DEBUG
        console.log "writing #{cs_out_file}, #{js_out_file}, and #{map_out_file}"

        # Need this comment directive for source map to work.
        js_output += "\n/*\n//@ sourceMappingURL=#{map_out_file}\n*/\n"
        fs.writeFileSync path.join(PH_OUTPUT_DIR, cs_out_file), src_contents, 'utf-8'
        fs.writeFileSync path.join(PH_OUTPUT_DIR, js_out_file), js_output, 'utf-8'
        fs.writeFileSync path.join(PH_OUTPUT_DIR, map_out_file), sm_output, 'utf-8'
    else
        console.log "writing #{js_out_file}"
        fs.writeFileSync path.join(PH_OUTPUT_DIR, js_out_file), js_output, 'utf-8'


task "clean", "Clean up build directory.", ->
    do clean_dir = (dir_path=PH_OUTPUT_DIR, rmdir=false) ->
        try
            files = fs.readdirSync dir_path
        catch e
            return

        for file in files
            file_path = path.join dir_path, file
            if fs.statSync(file_path).isFile()
                fs.unlinkSync file_path
              else
                clean_dir file_path, true

        if rmdir
            fs.rmdirSync dir_path


task "rebuild", "Clean build directory and build.", ->
    invoke "clean"
    invoke "build"


# Windows has an aneurysm with buffered stdout/stderr not flushing on process exit.
# https://gist.github.com/cowboy/3427148
exit = (exit_code) ->
    if process.stdout._pendingWriteReqs or process.stderr._pendingWriteReqs
        process.nextTick ->
            exit exit_code
    else
        process.exit exit_code