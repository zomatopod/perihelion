#_require conf
#_require base
#_require physics
#_require event
#_require steering
#_require util
#_require sound

# TODO: Objects is probably too close to built-in Object.
Objects = {}
Actors = {}


class Objects.Entity extends mixOf PIXI.MovieClip, Destructable, EventListenable
    texture_name: "no_texture_defined"
    sprite_size: [null, null]

    sound_create: null
    sound_destroy: null

    _entity_texture_shared: {}

    constructor: (attrs={}, initial_position=null) ->
        this.add_listener DestroyAllEntitiesEvent, (event) =>
            this.destroy()

        # Attrs overrides any prototyped properties.
        for attr, value of attrs when attr of this
            this[attr] = value

        # @texture_name can be an array, where we will select a random name.
        select_texture_name = if Array.isArray(@texture_name) then Util.random_choice(@texture_name) else @texture_name

        # Cache textures used by this entity class on the constructor's prototype
        # to be shared by all child instances.
        if select_texture_name not of @_entity_texture_shared
            @constructor::_entity_texture_shared[select_texture_name] = AssetManager.new_texture select_texture_name

        texture = @_entity_texture_shared[select_texture_name]

        # Call super with texture.
        if texture.animated?
            PIXI.MovieClip.constructor.call(this, texture)
        else
            PIXI.MovieClip.constructor.call(this, [texture])

        @width = @sprite_size[0] or @width
        @height = @sprite_size[1] or @height
        @anchor.x = 0.5
        @anchor.y = 0.5

        if initial_position
            @position.x = initial_position[0]
            @position.y = initial_position[1]

        Sound.emit @sound_create, (if initial_position then initial_position else null)

    __destroy_entity: ->
        Sound.emit @sound_destroy, vec2.fromValues(@position.x, @position.y)


class Objects.GenericEntity extends Objects.Entity
    constructor: (attrs, initial_position) ->
        super attrs, initial_position
        this.add_listener TickEvent, this.update
        EventDispatcher.queue_event new EntityCreateEvent(this)

    update: (event) =>
        null

    __destroy_generic_entity: ->
        EventDispatcher.queue_event new EntityDestroyEvent(this)


class Objects.Actor extends Objects.Entity
    hp: 1

    body_size: [null, null]
    body_mass: 4

    sound_damage: null

    constructor: (attrs, initial_position) ->
        super attrs, initial_position
        this.add_listener TickEvent, @update
        this.add_listener BodyCollisionEvent, @collision_callback

        @max_hp = @hp
        @alive = true

        # If rigidbody is not explictly defined, then it will default to sprite dimensions.
        body_width  = @body_size[0] or @width
        body_height = @body_size[1] or @height
        @body = new Physics.VerletRectBody {cx:0, cy:0, width: body_width, height: body_height}, @body_mass
        @body.set_position(initial_position) if initial_position
        @body.data.actor = this

        @update_sprite_rotation = true
        @update_sprite_position = true

        EventDispatcher.queue_event new ActorCreateEvent this

    __destroy_actor: (args...) ->
        EventDispatcher.queue_event new ActorDestroyEvent this, (args[0] or false)
        @alive = false

    # Overrides base method.
    updateTransform: ->
        if @update_sprite_position
            @position.x = @body.affix_point[0]
            @position.y = @body.affix_point[1]
        if @update_sprite_rotation
            @rotation = @body.rotation

        super()

    update: (event) =>
        null

    collision_callback: (event) =>
        if event.body == @body
            @_on_collision event.collider, event.manifold

    _on_collision: (collider, manifold) ->
        null

    damage: (points, vector) ->
        Sound.emit @sound_damage, @body.position

        @hp -= points
        this.destroy() if @hp <= 0

    heal: (points) ->
        @hp = Math.min @max_hp, @hp + points

    remove: ->
        @removed_from_play = true
        this.destroy(true)

    draw_debug: (context, transform) ->
        @body.draw_debug context, transform


class Objects.BaseProjectile extends Objects.Actor
    lifetime: 2
    damage: 1

    constructor: (pos, angle, muzzle_speed, attrs={}, ignore=null) ->
        super attrs, pos

        @body.collision_class = 1
        @body.collision_ignore = 1
        @body.collision_ignore_object = ignore
        @body.set_rotation angle
        @body.displace Util.vector_from_angle(vec2.create(), angle, muzzle_speed)

        @target = null
        @play()

    update: (event) =>
        delta_time = event.delta_time
        # Remove projectile if it's alive too long.
        @lifetime -= delta_time
        if @lifetime <= 0
            this.destroy()

    _on_collision: (collider, manifold) ->
        # Apply damage against target.
        if collider.data.actor?
            collider.data.actor.damage @damage, @body.velocity

        this.destroy()


class Objects.BaseMissile extends Objects.BaseProjectile
    impulse: 200
    speed: 20
    turn_speed: 2

    sound_loop: "rocket_drone"

    constructor: (pos, angle, muzzle_speed, attrs={}, ignore=null) ->
        super pos, angle, muzzle_speed, attrs, ignore

        @engine_drone_emitter = new Sound.Emitter {
            falloff_max: 50
            falloff_ref: 1
            falloff_factor: 1.0
        }

        @face = new Steering.Face()
        @face.max_rotational_speed = @turn_speed
        @face.max_angular_accel = 10
        @thrust_vec = vec2.create()

        new Effects.EngineFlare this
        new Effects.SmokeTrail this

        Effects.launch pos

        @body.max_speed = @speed
        @engine_drone_emitter.loop @sound_loop

    update: (event) =>
        delta_time = event.delta_time

        # Thrust forward at current heading.
        Util.vector_from_angle @thrust_vec, @body.rotation, @impulse
        @body.add_force @thrust_vec

        if @target
            @face.steer this.body, @target.body
            @body.set_rotation @body.rotation + (@face.angular * delta_time)
            @body.clear_angular_velocity()

        @engine_drone_emitter.update_loops @body.position
        super event

    __destroy_base_missile: ->
        @engine_drone_emitter.stop_loops()
        Effects.small_explosion @body.position


class WeaponSystem
    _vector_pool: Util.create_vector_pool 5

    @IDLING: 0
    @WAITING: 1
    @EXPENDING_SALVO: 2
    @RESET: 3

    constructor: (@owner_actor, attrs={}, @fire_callback=this.default_fire) ->
        @muzzle_speed = attrs.muzzle_speed or 50
        @salvo_delay = attrs.salvo_delay or 0
        @salvo = attrs.salvo or 1
        @projectile_class = attrs.projectile_class or BaseProjectile
        @projectile_attrs = attrs.projectile_attrs or {}
        @fire_callback = attrs.fire or WeaponSystem.default_fire
        attrs.rate = attrs.rate or 10

        @firing = false
        @fire_info = {}

        @_fire_step = 1.0 / attrs.rate
        @_fire_ctr = @_fire_step        # Primed to immediately fire.
        @_salvo_wait = @salvo_delay     # Ditto.
        @_salvo_ct = 0

        @_state = WeaponSystem.IDLING

    fire_cycle: (delta_time, angle, target=null) ->
        @fire_info.delta_time = delta_time
        @fire_info.angle = angle
        @fire_info.target = target
        @fire_info.weapon_system = this
        @fire_info.owner_actor = @owner_actor
        @fire_info.percentage = 0

        # Fire counter is never reset and always accumulates
        # regardless if the weapon is currently firing or not so
        # the player cannot cheat the cyclic rate by mashing the
        # fire button. fire_cycle() should be called every frame.
        @_fire_ctr += delta_time

        if @_state != WeaponSystem.EXPENDING_SALVO
            # Clamp to ready firing. If not, then the fire counter will
            # continually accumulate and madness occurs when firing begins...
            @_fire_ctr = Math.min @_fire_ctr, @_fire_step

        #### Weapon firing states. ####

        if @_state == WeaponSystem.IDLING
            @_salvo_wait += delta_time
            if @firing and (@_salvo_wait >= @salvo_delay)
                @_state = WeaponSystem.EXPENDING_SALVO

        if @_state == WeaponSystem.EXPENDING_SALVO
            # Fire required number of times to meet fire rate.
            while @_salvo_ct < @salvo and @_fire_ctr > @_fire_step
                @_fire_ctr -= @_fire_step
                @_salvo_ct += 1

                # Percentage is how far we are into the next firing cycle,
                # used to interpolate initial projectile position.
                @fire_info.percentage = @_fire_ctr / @_fire_step
                @fire_callback this, @fire_info

            # Finished firing salvo.
            if @_salvo_ct == @salvo
                @_state = WeaponSystem.RESET

            # Actor canceled firing before the first salvo shot has
            # been fired -- abort the entire firing sequence.
            if @_salvo_ct == 0 and not @firing
                @_state = WeaponSystem.RESET

        if @_state == WeaponSystem.RESET
            @_salvo_ct = 0
            @_salvo_wait = 0
            @_state = WeaponSystem.IDLING

    fire_projectile: (fire_info) ->
        @_vector_pool.push()
        # Offset the initial projectile position by a percentage of the muzzle velocity.
        mv = Util.vector_from_angle @_vector_pool(), fire_info.angle, @muzzle_speed*fire_info.delta_time
        p = vec2.copy @_vector_pool(), fire_info.owner_actor.body.position
        vec2.add p, p, (vec2.scale mv, mv, fire_info.percentage)

        projectile = new @projectile_class(p, fire_info.angle, @muzzle_speed, @projectile_attrs, fire_info.owner_actor.body)
        if fire_info.target then projectile.target = fire_info.target

        @_vector_pool.pop()

    @default_fire: (weapon_system, fire_info) ->
        weapon_system.fire_projectile fire_info


class ActorCreateEvent extends Event
    event_name: "actor_create"
    constructor: (@actor) ->


class ActorDestroyEvent extends Event
    event_name: "actor_destroy"
    constructor: (@actor, @is_removed=false) ->


class EntityCreateEvent extends Event
    event_name: "entity_create"
    constructor: (@entity) ->


class EntityDestroyEvent extends Event
    event_name: "entity_destroy"
    constructor: (@entity) ->


class DestroyAllEntitiesEvent extends Event
    event_name: "destroy_all_entities"


do ->
    _entites_in_play = 0
    _player_actor = null

    EventDispatcher.add_listener ActorCreateEvent, (event) =>
        _entites_in_play += 1

    EventDispatcher.add_listener ActorDestroyEvent, (event) =>
        _entites_in_play -= 1

    EventDispatcher.add_listener EntityCreateEvent, (event) =>
        _entites_in_play += 1

    EventDispatcher.add_listener EntityDestroyEvent, (event) =>
        _entites_in_play -= 1

    Objects.destroy_all_entities = ->
        while _entites_in_play > 0
            EventDispatcher.trigger new DestroyAllEntitiesEvent()
            EventDispatcher.dispatch()

    Objects.set_player_actor = (actor) ->
        _player_actor = actor

    Objects.get_player_actor = ->
        return _player_actor

    Objects.delete_player_actor = ->
        _player_actor = null
