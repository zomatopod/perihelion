#_require objects
#_require input
#_require util
#_require steering
#_require effects
#_require sound

_weapon_defs = ->
    return {
        "standard": {
            muzzle_speed: 50
            rate: 4
            lifetime: 2
            projectile_class: StandardProjectile
        }
    }


class Actors.Player extends mixOf Objects.Actor, SoundListener
    texture_name: "player"
    sprite_size: [2, 2]
    body_size: [1.5, 1.5]
    body_mass: 4
    hp: 8

    sound_damage: "hit"

    constructor: (@bounds_width, @bounds_height) ->
        super()

        if Objects.get_player_actor() != null
            throw new Error "A player actor already exists."

        Objects.set_player_actor this

        @regular_speed = 20
        @body.max_speed = @regular_speed

        @thrust_impulse = 1200
        @thrust_keys = 0
        @thrust_vector = vec2.create()

        @hp_regen_rate = 0.05

        @alignment = new Steering.Align()
        @alignment.max_angular_accel = 3.0
        @alignment.max_rotational_speed = 3.0

        @fire_vec = vec2.create()
        @weapon_systems = {}

        # Create WeaponSystem objects from the weapons definitions.
        for k,v of _weapon_defs()
            @weapon_systems[k] = new WeaponSystem this, v

        @current_weapon = @weapon_systems["standard"]

        this.add_listener InputKeydownEvent, @keydown_callback
        this.add_listener InputKeyupEvent, @keyup_callback
        this.add_listener InputMousedownEvent, @mousedown_callback
        this.add_listener InputMouseupEvent, @mouseup_callback
        this.add_sound_listener()

    __destroy_player: ->
        Objects.delete_player_actor()
        Effects.multisplosion @body.position, 13

    get_sound_listener_position: ->
        return @body.position

    update: (event) =>
        delta_time = event.delta_time

        # Movement vector from input.
        Input.MoveTable.get_8way_vector @thrust_vector, @thrust_keys
        vec2.scale @thrust_vector, @thrust_vector, @thrust_impulse * delta_time

        # Apply movement impulse.
        if vec2.sqrLen(@thrust_vector) > 0.0
            @body.add_force vec2.scale(@thrust_vector, @thrust_vector, @body.mass)

            # Turn sprite towards the movement direction.
            @alignment.steer @body, Math.atan2(@thrust_vector[1], @thrust_vector[0])
            @body.set_rotation @body.rotation + (@alignment.angular * delta_time)
            @body.clear_angular_velocity()

        # Cycle weapons at the direction of the cursor.
        vec2.sub @fire_vec, @parent.mouse_position, @body.position
        direction = Math.atan2(@fire_vec[1], @fire_vec[0])
        for _, weapon of @weapon_systems
            weapon.fire_cycle delta_time, direction

        # Speed clamping. This will steadily decrease the max speed down
        # to the regular speed in cases when max has been temporarily increased
        # for a burst of impulse (e.g. reaction to damage, afterburners).
        @body.max_speed -= 10 * delta_time
        @body.max_speed = Math.min @body.max_speed, vec2.length(@body.velocity)
        @body.max_speed = Math.max @body.max_speed, @regular_speed

        if @hp < @max_hp
            this.heal @hp_regen_rate * delta_time

        this.constrain_to_world()

    constrain_to_world: ->
        bounds_halfwidth = @bounds_width * 0.5
        bounds_halfheight = @bounds_height * 0.5
        halfwidth = @width * 0.5
        halfheight = @height * 0.5

        x = @body.position[0]
        y = @body.position[1]
        x = Math.max -(bounds_halfwidth - halfwidth), x   # lower bound
        x = Math.min  (bounds_halfwidth - halfwidth), x   # upper bound
        y = Math.max -(bounds_halfheight - halfheight), y # lower bound
        y = Math.min  (bounds_halfheight - halfheight), y # upper bound

        @body.set_position vec2.fromValues(x, y)

    damage: (points, vector) ->
        # Lose any fractional, regenerating hp.
        @hp = Math.floor(@hp)
        super points

        @body.max_speed = 50
        @body.add_force vec2.scale(vec2.create(), vector, 100)

        EventDispatcher.queue_event new PlayerDamageEvent(this, points)
        new Effects.ExplosionShockwave @body.position

    heal: (points) ->
        super points
        EventDispatcher.queue_event new PlayerHealEvent(this, points)

    keydown_callback: (event) =>
        @thrust_keys |= switch event.keycode
            when Input.keycodes.key_a
                Input.MoveTable.key_left
            when Input.keycodes.key_d
                Input.MoveTable.key_right
            when Input.keycodes.key_w
                Input.MoveTable.key_up
            when Input.keycodes.key_s
                Input.MoveTable.key_down

    keyup_callback: (event) =>
        @thrust_keys &= ~ switch event.keycode
            when Input.keycodes.key_a
                Input.MoveTable.key_left
            when Input.keycodes.key_d
                Input.MoveTable.key_right
            when Input.keycodes.key_w
                Input.MoveTable.key_up
            when Input.keycodes.key_s
                Input.MoveTable.key_down

    mousedown_callback: (event) =>
        @current_weapon.firing = true

    mouseup_callback: (event) =>
        @current_weapon.firing = false


class StandardProjectile extends Objects.BaseProjectile
    texture_name: "burn"
    sprite_size: [4, 4]
    body_size: [1.5, 1.5]

    sound_create: "shoot"


class PlayerDamageEvent extends Event
    event_name: "player_damage"
    constructor: (@player, @points) ->


class PlayerHealEvent extends Event
    event_name: "player_heal"
    constructor: (@player, @points) ->
