#_require base


AssetManager = {}


class AssetManagerError extends Error
    @NO_ERROR: "No error."
    @JSON_MALFORMED: "JSON manifest is malformed."
    @JSON_LOADING_FAIL: "JSON manifest failed to load."
    @NO_ASSETS: "No appropriate assets were defined."
    @ASSET_LOADING_FAIL: "Asset failed to load."

    @TEXTURE_INVALID_DIMENSIONS: "Invalid dimensions for animated base texture."

    name: "AssetManagerError"

    constructor: (@src, @type) ->
        @message = "#{if @src then @src+'--' else ''}#{type}"


do ->
    _base_textures = {}

    _error_img = new Image()
    _error_img.src = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAIAAACQkWg2AAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAABnSURBVDhPY3wro8JACmCC0kQDkjUgnCT85A6EgRXAlaHYgNU/aIIoGoCWoEkDuWg2o/sBWQ+maiDA4mmIHqyqgYDkUMKiAWI2stuQAboGZJdg1YOiAdPdmHpQNKCphgA0wUGX+BgYAOO+MKVkUDuNAAAAAElFTkSuQmCC"
    _error_texture = new PIXI.BaseTexture _error_img

    _strip_ext = (str) -> str.replace(/\.[^/.]+$/, "")
    _join_path = (args...) ->
        full_path = ""

        for path in args
            path = (path or "").trim()
            if path == "" then continue

            (path += "/") if path[path.length - 1] != "/"       # Ensure trailing slash.
            full_path += path

        return full_path

    AssetManager.load_manifest_file = (manifest_file, callbacks={}) ->
        failure_callback = callbacks.failure or (error) -> throw error

        manifest_file = "#{manifest_file}.json"
        load_json = new createjs.LoadQueue()

        load_json.addEventListener "complete", ->
            manifest = load_json.getResult manifest_file

            if manifest instanceof SyntaxError
                console.log "#{manifest_file} is malformed."
                failure_callback new AssetManagerError(manifest_file, AssetManagerError.JSON_MALFORMED)

            else
                AssetManager.load manifest, callbacks

        load_json.addEventListener "error", ->
            console.log "#{manifest_file} failed to load."
            failure_callback new AssetManagerError(manifest_file, AssetManagerError.JSON_LOADING_FAIL)

        load_json.loadFile manifest_file

    AssetManager.load = (master_manifest, callbacks={}) ->
        # Optional defaults.
        loadstart_callback = callbacks.loadstart or (asset_ct) -> null
        success_callback = callbacks.success or () -> null
        fileload_callback = callbacks.fileload or (src) ->  null
        progress_callback = callbacks.progress or (percent) -> null
        failure_callback = callbacks.failure or (error) -> throw error

        load_queue = new createjs.LoadQueue()
        load_queue.setMaxConnections 4
        load_manifest = []

        load_queue.installPlugin createjs.Sound


        ### BUILD LOADING MANIFEST LIST ###

        # Currently, as of PreloadJS 0.3.1 and SoundJS 0.4.1, sounds to not work correctly when
        # preloaded with basepath set in LoadQueue (for most browsers except Firefox for some reason).
        # So, we won't set a LoadQueue basepath and manually prepend it to src instead.
        # http://community.createjs.com/discussions/soundjs/214-webaudio-not-playing-if-used-with-a-path-in-the-loadqueue

        texture_path = _join_path  master_manifest["base path"], master_manifest["texture path"]
        for texture_entry in master_manifest.textures or []
            item = {
                type: createjs.LoadQueue.IMAGE
                data: {}
            }

            if typeof texture_entry == "string"
                item.id = _strip_ext texture_entry
                item.src = texture_path + texture_entry
            # Animated texture.
            else
                [file, item.data.anim_columns, item.data.anim_rows] = texture_entry
                item.id = _strip_ext file
                item.src = texture_path + file
                item.data.animated = true

            load_manifest.push item

        sound_path = _join_path  master_manifest["base path"], master_manifest["sound path"]
        for sound_entry in master_manifest.sounds or []
            load_manifest.push {
                id: _strip_ext sound_entry
                type: "sound"
                src: (sound_path + sub_item.trim() for sub_item in sound_entry.split("|")).join("|")
            }


        ### LOAD QUEUE EVENT CALLBACKS ###

        load_queue.addEventListener "fileload", (event) =>
            if load_queue.failure then return

            item = event.item
            result = event.result

            type = item.type
            id = item.id
            src = item.src
            data = item.data

            # Set asset to appropriate manager property.
            switch type
                when createjs.LoadQueue.IMAGE
                    base_tex = new PIXI.BaseTexture result
                    if item.data.animated?
                        base_tex.anim_columns = item.data.anim_columns
                        base_tex.anim_rows = item.data.anim_rows
                        base_tex.animated = true
                    _base_textures[id] = base_tex

                when "sound"
                    null

            console.log "Loaded #{src}."
            fileload_callback src

        load_queue.addEventListener "loadstart", =>
            console.log "Loading #{load_manifest.length} asset(s)."
            loadstart_callback load_manifest.length

        load_queue.addEventListener "progress", (event) ->
            if load_queue.failure then return
            progress_callback event.progress

        load_queue.addEventListener "complete", =>
            console.log "Finished loading."
            success_callback()

        load_queue.addEventListener "error", (event) ->
            item = event.item
            result = event.result

            type = item.type
            id = item.id
            src = item.src
            data = item.data

            # Terminate loading process if an essential asset failed to load.
            load_queue.close()
            load_queue.failure = true

            console.log "Failed to load #{src} as #{type}."
            failure_callback new AssetManagerError(src, AssetManagerError.ASSET_LOADING_FAIL)


        ### FINALLY... ###
        # After all the setup, we base_texturehave to actually fire off the loading process.

        if load_manifest.length > 0
            load_queue.failure = false
            load_queue.loadManifest load_manifest, true#, basepath
        else
            failure_callback new AssetManagerError(null, AssetManagerError.NO_ASSETS)


    # Right now, frame will be ignored if the requested base texture is animated.
    AssetManager.new_texture = (name, frame=null) ->
        base_tex = _base_textures[name]

        if not base_tex
            # throw new AssetManagerError "No base texture '#{name}' found."
            console.log "No base texture '#{name}' found."
            return new PIXI.Texture _error_texture

        if base_tex.animated?
             # Return an array of textures sliced with frames of a base texture.
            textures = []
            textures.animated = true

            tex_width = Math.floor(base_tex.width / base_tex.anim_columns)
            tex_height = Math.floor(base_tex.height / base_tex.anim_rows)

            for y in [0...base_tex.height] by tex_height
                for x in [0...base_tex.width] by tex_width
                    try
                        frame = new PIXI.Rectangle x, y, tex_width, tex_height
                        tex = new PIXI.Texture base_tex, frame
                        textures.push tex
                    catch e
                        throw new AssetManagerError(name, AssetManagerError.TEXTURE_INVALID_DIMENSIONS)

            return textures

        else
            return new PIXI.Texture base_tex, frame
