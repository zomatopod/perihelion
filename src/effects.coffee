#_require objects
#_require util
#_require sound


Effects = {}


do ->
    _explosion_sound_emitter = new Sound.Emitter {
        falloff_max: 150
        falloff_ref: 30
        falloff_factor: 0.5
    }

    _small_explosion_sound_emitter = new Sound.Emitter {
        falloff_max: 50
        falloff_ref: 10
        falloff_factor: 1.0
    }

    Effects.explosion = (pos, delay=0) ->
        opts = {
            pos_variance: 0
            size_variance: 0
            speed: 0
            start_delay: 0
            expansion_rate: 0
        }

        # Main fireball and shockwave.
        opts.start_delay = delay
        new Effects.ExplosionFireball pos, 40, opts
        new Effects.ExplosionShockwave pos, opts

        # Drifing fireballs
        opts.start_delay = delay
        opts.pos_variance = 0
        opts.size_variance = 5
        for _ in [0...Util.random_range_int(5,8)]
            opts.speed = 30
            new Effects.ExplosionFireball pos, 20, opts

        # Sparks -- delayed all at once and then fired
        # in rapid succession.
        opts.speed = 0
        opts.start_delay = delay + 0.3
        opts.pos_variance = 15
        opts.size_variance = 0
        for _ in [0...Util.random_range_int(5,10)]
            opts.start_delay += Util.random_range 0.0, 0.1
            new Effects.ExplosionSparks pos, 20, opts

        # Ancilliary fireballs.
        opts.speed = 0
        opts.pos_variance = 10
        opts.size_variance = 5
        for _ in [0...Util.random_range_int(4,6)]
            opts.start_delay = delay + (Util.random_range 0.0, 0.5)
            new Effects.ExplosionFireball pos, 30, opts

        _explosion_sound_emitter.emit Util.random_choice(["explode1", "explode2"]), pos


    Effects.multisplosion = (pos, number) ->
        delay = 0
        rnd_pos = vec2.create()

        for i in [0...number]
            vec2.random rnd_pos, Math.random() * 13
            vec2.add rnd_pos, rnd_pos, pos
            Effects.explosion rnd_pos, delay
            delay += 0.25


    Effects.small_explosion = (pos, delay=0) ->
        opts = {
            pos_variance: 0,
            size_variance: 0,
            speed: 0,
            start_delay: 0,
            expansion: 0,
        }

        # Main fireball and shockwave.
        opts.start_delay = delay
        new Effects.ExplosionFireball pos, 20, opts

        # Drifing fireballs
        opts.start_delay = delay
        opts.pos_variance = 0
        opts.size_variance = 5
        for _ in [0...Util.random_range_int(4,7)]
            opts.speed = 30
            new Effects.ExplosionFireball pos, 10, opts

        _small_explosion_sound_emitter.emit Util.random_choice(["small_explode1", "small_explode2"])


    Effects.launch = (pos, angle) ->
        # angle += Math.PI
        opts = {
            pos_variance: 0
            size_variance: 0
            speed: 10
            start_delay: 0
            expansion_rate: 0
        }

        for _ in [0...Util.random_range_int(5,8)]
            new Effects.ExplosionFireball pos, 10, opts


class Effects.ExplosionElement extends Objects.GenericEntity
    pos_variance: 0.0
    size_variance: 0.0
    speed: 0.0
    expansion_rate: 0.0
    rotation_rate: 0.0
    start_delay: 0.0

    constructor: (pos, size=1.0, attrs={}) ->
        super attrs

        # Size variance.
        size -= (@size_variance * 0.5) + (Math.random() * @size_variance)
        @width = @height = size

        # Position offset from center based on random variance factor.
        rnd_offset = vec2.random vec2.create(), Math.random() * @pos_variance
        @position.x = pos[0] + rnd_offset[0]
        @position.y = pos[1] + rnd_offset[1]
        @rotation = Math.random() * Math.PI * 2.0

        @drift_velocity = vec2.random vec2.create(), @speed

        # MovieClip animation properties.
        @loop = false
        @onComplete = =>
            this.destroy()

        @_start_time = 0

    update: (event) =>
        delta_time = event.delta_time

        # Start animation after start delay is finished.
        if @_start_time > -1
            @_start_time += delta_time
            if @_start_time >= @start_delay
                this.play()
                @_start_time = -1

        # Drift and expand sprite after animation has started.
        else
            @position.x += @drift_velocity[0] * delta_time
            @position.y += @drift_velocity[1] * delta_time
            @width += @expansion_rate * delta_time
            @height += @expansion_rate * delta_time
            @rotation += @rotation_rate * delta_time


class Effects.ExplosionSparks extends Effects.ExplosionElement
    texture_name: "fastsparks"

    constructor: (pos, size, attrs) ->
        super pos, size, attrs
        @animationSpeed = 0.5


class Effects.ExplosionFireball extends Effects.ExplosionElement
    texture_name: ["fireball1", "fireball2", "fireball3", "fireball4"]

    constructor: (pos, size, attrs) ->
        super pos, size, attrs


class Effects.ExplosionShockwave extends Effects.ExplosionElement
    texture_name: "shockwave"

    constructor: (pos, attrs={}) ->
        super pos, 10, {
            start_delay: attrs.start_delay or 0
            expansion_rate: 400
        }

        @animationSpeed = 0.05
        @width = 0
        @height = 0


class Effects.SmallSplosion extends Objects.GenericEntity
    texture_name: ["expl1", "expl2"]
    sprite_size: [4, 4]

    constructor: (pos) ->
        super()

        @position.x = pos[0]
        @position.y = pos[1]

        # MovieClip animation properties.
        @loop = false
        @onComplete = =>
            this.destroy()

        this.play()


class Effects.Debris extends Objects.GenericEntity
    texture_name: "debris"
    sprite_size: [1, 1]

    constructor: (pos) ->
        super()
        @drift_velocity = vec2.random vec2.create(), 6
        @angular_velocity = Math.random()*3 - 3
        @lifetime = Math.random()*3 + 3

        @texture = Util.random_choice @textures
        @position.x = pos[0]
        @position.y = pos[1]

    update: (event) =>
        delta_time = event.delta_time

        @position.x += @drift_velocity[0] * delta_time
        @position.y += @drift_velocity[1] * delta_time
        @rotation += @angular_velocity * delta_time

        @lifetime -= delta_time
        if @lifetime <= 0
            new Effects.SmallSplosion vec2.fromValues(@position.x, @position.y)
            this.destroy()


class Effects.AffixedEffect extends Objects.GenericEntity
    constructor: (@parent_actor, attrs={}) ->
        super attrs
        this.add_listener ActorDestroyEvent, @actor_destroy_callback
        @affix = vec2.fromValues -(@parent_actor.width * 0.5), 0.0

    update: (event) =>
        delta_time = event.delta_time

        m = mat3.create()
        p = vec2.clone @affix
        mat3.rotate m, m, @parent_actor.body.rotation
        vec2.transformMat3 p, p, m

        @position.x = @parent_actor.body.position[0] + p[0]
        @position.y = @parent_actor.body.position[1] + p[1]

    actor_destroy_callback: (event) =>
        if event.actor == @parent_actor
            delete @parent_actor
            this.destroy()


class Effects.EngineFlare extends Effects.AffixedEffect
    texture_name: "burn"
    sprite_size: [30, 2]

    constructor: (parent_actor) ->
        super parent_actor

    update: (event) =>
        super event
        @alpha = Math.random()


class Effects.SmokeTrail extends Effects.AffixedEffect
    texture_name: "smoke"
    sprite_size: [0.1, 0.1]

    constructor: (parent_actor) ->
        super parent_actor
        @create_smoke = 0

    update: (event) =>
        @create_smoke += event.delta_time
        if @create_smoke > 0.075
            new Effects.SmokeTrailParticle @parent_actor.body.position
            @create_smoke = 0


class Effects.SmokeTrailParticle extends Objects.GenericEntity
    texture_name: "smoke"
    sprite_size: [0.1, 0.1]

    constructor: (pos) ->
        super()
        @position.x = pos[0]
        @position.y = pos[1]

        this.play()

    update: (event) =>
        delta_time = event.delta_time
        @alpha = Math.max 0, @alpha - delta_time * 0.5
        @width = @height += delta_time * 5.0
        @rotation += delta_time * 3.0
        if @alpha == 0
            this.destroy()


class Effects.MotionField extends Destructable
    constructor: (num_particles, @width, @height) ->
        @last_position = vec2.create()
        @halfwidth = @width * 0.5
        @halfheight = @height * 0.5
        @particles = []

        for i in [0...num_particles]
            p = new Effects.MotionFieldParticle Util.random_range(-@halfwidth, @halfwidth), Util.random_range(-@halfheight, @halfheight)
            @particles.push p

    __destroy_motionfield: ->
        particle.destroy() for particle in @particles

    set_new_position: (new_position) ->
        d = vec2.sub(vec2.create(), new_position, @last_position)
        speed = vec2.length d
        direction = Util.angle_from_vector d
        x = new_position[0]
        y = new_position[1]

        for p in @particles
            p.set_new_position speed, direction
            while this.wrap p, x, y
                null

        vec2.copy @last_position, new_position

    wrap: (p, x, y) ->
        r = false
        if p.position.x > x+@halfwidth
            p.position.x -= @width
            r = true
        if p.position.x < x-@halfwidth
            p.position.x += @width
            r = true
        if p.position.y > y+@halfheight
            p.position.y -= @height
            r = true
        if p.position.y < y-@halfheight
            p.position.y += @height
            r = true

        return r


class Effects.MotionFieldParticle extends Objects.GenericEntity
    texture_name: "point"
    sprite_size: [0.1, 0.1]

    max_speed: 0.5
    max_length: 1.0
    max_alpha: 0.2

    constructor: (x, y) ->
        super()
        @position.x = x
        @position.y = y

    set_new_position: (speed, direction) ->
        percent = (Math.min speed, @max_speed) / @max_speed
        @width = @max_length * percent
        @alpha = @max_alpha * percent
        @rotation = direction
