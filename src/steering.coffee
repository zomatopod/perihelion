#_require util.coffee


Steering = {}

class Steering.BaseSteering
    constructor: ->
        @linear = new vec2.create()
        @angular = 0.0

        @max_rotational_speed = 1.0

        @max_acceleration = 10
        @max_angular_accel = 0.6

        @time_to_target = 0.75

    steer: (source, target) ->

    debug_draw: (context, transform) ->


class Steering.Seek extends Steering.BaseSteering
    constructor: ->
        super()

    steer: (source, target) ->
        vec2.sub @linear, target.position, source.position
        vec2.normalize @linear, @linear

        vec2.scale @linear, @linear, @max_acceleration
        @angular = 0


class Steering.Arrive extends Steering.Seek
    constructor: ->
        super()
        @target_radius = 5.0
        @slow_radius = 10.0

    steer: (source, target) ->
        direction = vec2.sub vec2.create(), target.position, source.position
        distance = vec2.length direction
        speed = 0.0

        # At target
        if distance < @target_radius
            vec2.set @linear, 0.0, 0.0
            return

        # Slow down if inside the slow radius.
        if distance < @slow_radius
            speed = source.max_speed * distance / @slow_radius
        else
            speed = source.max_speed

        objective_velocity = vec2.clone direction
        vec2.normalize objective_velocity, objective_velocity
        vec2.scale objective_velocity, objective_velocity, speed

        # Accelerate to objective velocity
        vec2.sub @linear, objective_velocity, source.velocity
        vec2.scale @linear, @linear, 1.0/@time_to_target

        # Clamp to max acceleration.
        if vec2.sqrLen(@linear) > (@max_acceleration*@max_acceleration)
            vec2.normalize @linear, @linear
            vec2.scale @linear, @linear, @max_acceleration

        @angular = 0


class Steering.Align extends Steering.BaseSteering
    constructor: ->
        super()
        @target_radius = 0.01
        @slow_radius = 0.5

    steer: (source, target) ->
        w1 = source.rotation
        w2 = if typeof target == "number" then target else target.rotation

        target_rotation = 0.0

        rotation_delta = Util.map_to_range(w2 - w1)
        rotation_delta_size = Math.abs rotation_delta

        # At target.
        if rotation_delta_size < @target_radius
            @angular = 0.0
            return

        # Slow down if inside the slow radius.
        if rotation_delta_size < @slow_radius
            target_rotation = @max_rotational_speed * rotation_delta_size / @slow_radius
        else
            target_rotation = @max_rotational_speed

        # Flip back to correct sign.
        target_rotation = target_rotation * rotation_delta / rotation_delta_size

        # Accelerate to target rotation.
        @angular = target_rotation - source.angular_velocity

        # Clamp to max acceleration.
        angular_accel = Math.abs @angular
        if angular_accel > @max_angular_accel
            @angular /= angular_accel
            @angular *= @max_angular_accel


class Steering.Face extends Steering.Align
    steer: (source, target) ->
        super source, Math.atan2(target.position[1] - source.position[1], target.position[0] - source.position[0])


class Steering.Pursue extends Steering.Seek
    max_prediction: 20

    steer: (source, target) ->
        direction = vec2.sub vec2.create(), target.position, source.position
        distance = vec2.length direction
        speed = vec2.length source.velocity
        prediction = 0

        # If speed is too low to make good prediction.
        console.log speed, distance
        if speed < (distance / @max_prediction)
            prediction = @max_prediction
        else
            prediction = distance / speed

        proxy_target = {
            position: vec2.scale vec2.create(), target.velocity, prediction
        }

        super source, proxy_target
