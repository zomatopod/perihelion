#_require conf.coffee
#_require event.coffee


Input = {}
Input.MoveTable = {}


do ->
    _keys = Array 255
    _last_keys = Array 255

    _mouse_btns = Array 5
    _last_mouse_btns = Array 5

    _mouse_pos = vec2.create()
    _last_mouse_pos = vec2.create()

    for i in [0..._keys.length]
        _keys[i] = false
        _last_keys[i] = false

    for i in [0..._mouse_btns.length]
        _mouse_btns[i] = false
        _last_mouse_btns[i] = false

    Input.queue_input_events = ->
        # Dispatch appropriate events if the key state has been changed
        # since the last call.
        for i in [0..._keys.length]
            last_key = _last_keys[i]
            key = _keys[i]

            if last_key != key
                if key == true
                    EventDispatcher.queue_event new InputKeydownEvent(i)
                else
                    EventDispatcher.queue_event new InputKeyupEvent(i)

                _last_keys[i] = key

        # Same for mouse buttons.
        for i in [0..._mouse_btns.length]
            last_btn = _last_mouse_btns[i]
            btn = _mouse_btns[i]

            if last_btn != btn
                if btn == true
                    EventDispatcher.queue_event new InputMousedownEvent(i)
                else
                    EventDispatcher.queue_event new InputMouseupEvent(i)

                _last_mouse_btns[i] = btn

        # Mouse position
        if not Util.vec2_eq(_mouse_pos, _last_mouse_pos)
            delta = vec2.sub vec2.create(), _mouse_pos, _last_mouse_pos
            EventDispatcher.queue_event new InputMousemoveEvent vec2.clone(_mouse_pos), delta
            vec2.copy _last_mouse_pos, _mouse_pos

    document.onkeydown = (event) ->
        _keys[event.keyCode] = true

    document.onkeyup = (event) ->
        _keys[event.keyCode] = false

    document.onmousedown = (event) ->
        _mouse_btns[event.button] = true
        # Stops browser from performing hold-and-drag actions while the
        # mouse button is depressed.
        event.preventDefault()

    document.onmouseup = (event) ->
        _mouse_btns[event.button] = false

    document.onmousemove = (event) ->
        vec2.set _mouse_pos, event.clientX, event.clientY


do ->
    r = vec2.fromValues Math.cos(0), Math.sin(0)
    dr = vec2.fromValues Math.cos(Math.PI/4), Math.sin(Math.PI/4)
    d =  vec2.fromValues Math.cos(Math.PI/2), Math.sin(Math.PI/2)
    dl = vec2.fromValues Math.cos(Math.PI*3/4), Math.sin(Math.PI*3/4)
    l = vec2.fromValues Math.cos(Math.PI), Math.sin(Math.PI)
    ul = vec2.fromValues Math.cos(Math.PI*5/4), Math.sin(Math.PI*5/4)
    u = vec2.fromValues Math.cos(Math.PI*3/2), Math.sin(Math.PI*3/2)
    ur = vec2.fromValues Math.cos(Math.PI*7/4), Math.sin(Math.PI*7/4)
    none = vec2.create()

    Input.MoveTable.key_right = 1
    Input.MoveTable.key_up = 1 << 1
    Input.MoveTable.key_left = 1 << 2
    Input.MoveTable.key_down = 1 << 3

    Input.MoveTable.eight_way = [none, r, u, ur, l, none, ul, u, d, dr, none, r, dl, d, l, none]
    Input.MoveTable.four_way = [none, r, u, none, l, none, none, u, d, none, none, r, none, d, l, none]


    Input.MoveTable.get_8way_vector = (out, keys) ->
        if keys not of Input.MoveTable.eight_way
            throw new Error "Invalid keys -- expected a 16-bit value."
        return vec2.copy out, Input.MoveTable.eight_way[keys]


    Input.MoveTable.get_4way_vector = (out, keys) ->
        if keys not of Input.MoveTable.four_way
            throw new Error "Invalid keys -- expected a 16-bit value."
        return vec2.copy out, Input.MoveTable.four_way[keys]


Input.keycodes = {
    key_backspace: 8,
    key_tab:  9,
    key_enter: 13,
    key_shift: 16,
    key_ctrl: 17,
    key_alt: 18,
    key_pause: 19,
    key_capslock: 20,
    key_escape: 27,
    key_pageup: 33,
    key_pagedown: 34,
    key_end: 35,
    key_home: 36,
    key_left: 37,
    key_up: 38,
    key_right: 39,
    key_downt: 40,
    key_insert: 45,
    key_delete: 46,
    key_0: 48,
    key_1: 49,
    key_2: 50,
    key_3: 51,
    key_4: 52,
    key_5: 53,
    key_6: 54,
    key_7: 55,
    key_8: 56,
    key_9: 57,
    key_a: 65,
    key_b: 66,
    key_c: 67,
    key_d: 68,
    key_e: 69,
    key_f: 70,
    key_g: 71,
    key_h: 72,
    key_i: 73,
    key_j: 74,
    key_k: 75,
    key_l: 76,
    key_m: 77,
    key_n: 78,
    key_o: 79,
    key_p: 80,
    key_q: 81,
    key_r: 82,
    key_s: 83,
    key_t: 84,
    key_u: 85,
    key_v: 86,
    key_w: 87,
    key_x: 88,
    key_y: 89,
    key_z: 90,
    key_lwindows: 91,
    key_rwindows: 92,
    key_select: 93,
    key_numpad_0: 96,
    key_numpad_1: 97,
    key_numpad_2: 98,
    key_numpad_3: 99,
    key_numpad_4: 100,
    key_numpad_5: 101,
    key_numpad_6: 102,
    key_numpad_7: 103,
    key_numpad_8: 104,
    key_numpad_9: 105,
    key_multiply: 106,
    key_add: 107,
    key_subtract: 109,
    key_dot: 110,
    key_divide: 111,
    key_f1: 112,
    key_f2: 113,
    key_f3: 114,
    key_f4: 115,
    key_f5: 116,
    key_f6: 117,
    key_f7: 118,
    key_f8: 119,
    key_f9: 120,
    key_f10: 121,
    key_f11: 122,
    key_f12: 123,
    key_numlock: 144,
    key_scrolllock: 145,
    key_semicolon: 186,
    key_equal: 187,
    key_comma: 188,
    key_dash: 189,
    key_period: 190,
    key_slash: 191,
    key_backtick: 192,
    key_lbracket: 219,
    key_backslash:  220,
    key_rbracket: 221,
    key_singlequote: 222
}


class InputKeydownEvent extends Event
    event_name: "input_keydown"
    constructor: (@keycode) ->


class InputKeyupEvent extends Event
    event_name: "input_keyup"
    constructor: (@keycode) ->


class InputMousedownEvent extends Event
    event_name: "input_mousedown"
    constructor: (@button) ->


class InputMouseupEvent extends Event
    event_name: "input_mouseup"
    constructor: (@button) ->


class InputMousemoveEvent extends Event
    event_name: "input_mousemove"
    constructor: (@position, @delta) ->

    map_position_to_DOM: (out, element, constrain=true) ->
        r = element.getBoundingClientRect()
        vec2.set out, @position[0] - r.left, @position[1] - r.top

        if constrain
            out[0] = Math.max out[0], 0
            out[0] = Math.min out[0], r.width
            out[1] = Math.max out[1], 0
            out[1] = Math.min out[1], r.height

        return out