#_require conf
#_require collision
#_require util
#_require spatial
#_require event


Physics = {}


class PhysicsError extends Error
    name: "PhysicsError"
    constructor: (@message, @body=null) ->


do ->
    Physics.STEP_TIME = (1.0/PHYSICS_TICKRATE)

    _bodies = []
    _world_size = vec2.create()
    _quadtree = null
    _update_step = new Util.Accumulator Physics.STEP_TIME, PHYSICS_MAX_STEPS
    _init = false

    EventDispatcher.add_listener TickEvent, (event) ->
        if _init
            Physics.step event.delta_time

    Physics.init = (world_width, world_height) ->
        vec2.set _world_size, world_width, world_height
        _quadtree = new Quadtree {width: world_width, height: world_height, cx: 0, cy: 0}
        _bodies = []
        _update_step.clear()
        _init = true

    Physics.add_body = (body) ->
        if body not instanceof Physics.VerletBody
            throw new PhysicsError "Attempted to add a non-instance of Physics.VerletBody."

        # TODO: Check if the body has already been added -- adding a body twice does bad things.
        _bodies.push body

    Physics.remove_body = (body) ->
        i = _bodies.indexOf body
        if i > -1
            _bodies.splice i, 1
            return true
        else
            return false

    Physics.step = (delta_time) ->
        if not _init
            throw new PhysicsError "Initialize with Physics.init() first."

        iterations = _update_step.add delta_time

        if DEBUG and iterations < _update_step.overflow_amount
            req = _update_step.overflow_amount
            console.log "Physics required #{req} steps to update, clamped to #{iterations}."
            console.log "Simulation lagged #{Math.floor((req-iterations)*1000/PHYSICS_TICKRATE)} ms."

    _update_step.overflow_callback = ->
        _quadtree.clear()

        # Update bodies and AABBs, and populate quadtree for broadphase.
        for body in _bodies
            body.integrate Physics.STEP_TIME
            body.aabb.update_transform(null)
            _quadtree.insert body

        # Collision detection.
        for body in _bodies
            candidates = _quadtree.broad_query body.aabb
            for [candidate, candidate_bounds] in candidates when candidate != body
                if body.collision_ignore_object == candidate or candidate.collision_ignore_object == body then continue
                if body.collision_ignore & candidate.collision_class or candidate.collision_ignore & body.collision_class then continue

                # AABB culling -> narrowphase collision detection.
                if body.aabb.intersects candidate_bounds
                    manifold = Collision.create_manifold body, candidate
                    _resolve_contact manifold
                    EventDispatcher.queue_event new BodyCollisionEvent body, candidate, manifold

    _resolve_contact = (manifold) ->
        match_particle_from_vertex = (body, v) ->
            for p in body.points
                if p.position == v
                    return p
            throw new Error "Should not get here."

        contact = manifold.contacts[0]
        incident = contact.incident_ref

        if not contact.flip
            body_a = manifold.body_a
            body_b = manifold.body_b
        else
            body_a = manifold.body_b
            body_b = manifold.body_a

        n = vec2.scale vec2.create(), contact.normal, contact.depth
        neg_n = vec2.negate vec2.create(), n

        cp = match_particle_from_vertex body_a, contact.vertex_ref
        inc_p1 = match_particle_from_vertex body_b, incident[0]
        inc_p2 = match_particle_from_vertex body_b, incident[1]

        mass_a = cp.get_mass()
        mass_b = (inc_p1.get_mass() + inc_p2.get_mass()) * 0.5
        total_mass = mass_a + mass_b

        # Resolve contact point.
        # cp.set_position vec2.add(vec2.create(), cp.position, vec2.scale(vec2.create(), n, 0.5))
        cp.set_position vec2.add(vec2.create(), cp.position, vec2.scale(vec2.create(), n, mass_b/total_mass))

        # Resolve incident edge.
        if Math.abs(inc_p1.position[0] - inc_p2.position[0]) > Math.abs(inc_p1.position[1] - inc_p2.position[1])
            t = (contact.position[0] - neg_n[0] - inc_p1.position[0]) / (inc_p2.position[0] - inc_p1.position[0])
        else
            t = (contact.position[1] - neg_n[1] - inc_p1.position[1]) / (inc_p2.position[1] - inc_p1.position[1])

        lambda = 1.0 / (t*t + (1 - t) * (1 - t))

        # vec2.add inc_p1.position, inc_p1.position, vec2.scale(vec2.create(), neg_n, (1-t) * 0.5 * lambda)
        # vec2.add inc_p2.position, inc_p2.position, vec2.scale(vec2.create(), neg_n, t * 0.5 * lambda)
        vec2.add inc_p1.position, inc_p1.position, vec2.scale(vec2.create(), neg_n, (1-t) * (mass_a/total_mass) * lambda)
        vec2.add inc_p2.position, inc_p2.position, vec2.scale(vec2.create(), neg_n, t * (mass_a/total_mass) * lambda)

    Physics.draw_debug = (context, transform)->
        if DEBUG_DRAW_QUADTREE
            _quadtree.draw_debug context, transform


class Physics.VerletParticle
    _vector_pool: Util.create_vector_pool 10

    constructor: (x=0.0, y=0.0, mass=1.0) ->
        @position = vec2.fromValues x, y
        @last_position = vec2.clone @position

        @acceleration = vec2.create()
        @linear_accumulator = vec2.create()
        @_inv_mass = 1.0
        @set_mass mass

        # float m_bounce
        # float m_friction

    integrate: (delta_time) ->
        @_vector_pool.push()

        pooled_displace_term = @_vector_pool()
        pooled_velocity = @_vector_pool()

        # Newton's Second Law.
        # f=ma, a=f/m, a=f*(1/m)
        vec2.scale @acceleration, @linear_accumulator, @_inv_mass

        # x = x + (position - last_position) + acceleration * dt^2
        # x = x + (position - last_position)damping + acceleration * dt^2
        vec2.sub pooled_displace_term, @position, @last_position
        # vec2.scale pooled_displace_term, pooled_displace_term, damping
        vec2.scale pooled_velocity, @acceleration, delta_time*delta_time
        vec2.add pooled_velocity, pooled_displace_term, pooled_velocity

        # last_position = position
        # position += velocity
        vec2.copy @last_position, @position
        vec2.add @position, @position, pooled_velocity

        @clear_forces()
        @_vector_pool.pop()

    set_position: (pos_vec) ->
        @_vector_pool.push()
        # pooled_diff = position - last_position
        # position = new_position
        # last_position = position - pooled_diff
        pooled_diff = @_vector_pool()
        vec2.sub pooled_diff, @position, @last_position
        vec2.copy @position, pos_vec
        vec2.sub @last_position, @position, pooled_diff
        @_vector_pool.pop()

    set_displacement: (displace_vec, delta_time) ->
        @_vector_pool.push()
        # last_position = position + (pooled_displace * delta_time)
        pooled_displace = @_vector_pool()
        vec2.scale pooled_displace, displace_vec, delta_time
        vec2.add @last_position, @position, pooled_displace
        @_vector_pool.pop()

    clear_displacement: ->
        vec2.copy @last_position, @position

    add_force: (force_vec) ->
        vec2.add @linear_accumulator, @linear_accumulator, force_vec

    clear_forces: ->
        vec2.set @linear_accumulator, 0.0, 0.0

    set_mass: (mass) ->
        mass = Math.max 0.0, mass
        @_inv_mass = if mass > 0.0 then 1.0 / mass else 0.0

    get_mass: ->
        return 1.0 / @_inv_mass

    get_inv_mass: ->
        return @_inv_mass


class Physics.VerletConstraint
    _vector_pool: Util.create_vector_pool 10

    constructor: (@particle_a, @particle_b, @strength=1.0) ->
        @length = vec2.dist @particle_a.position, @particle_b.position

    constrain: (iterations=VERLET_CONSTRAINT_INTERATIONS) ->
        @_vector_pool.push()
        pooled_term_a = @_vector_pool()
        pooled_term_b = @_vector_pool()
        pooled_error = @_vector_pool()
        pooled_unit_delta = @_vector_pool()

        for i in [0...iterations]
            # Can constrain to bounds here
            inv_mass_a = @particle_a.get_inv_mass()
            inv_mass_b = @particle_b.get_inv_mass()

            # Two infinite-mass points = no-go.
            if inv_mass_a + inv_mass_b == 0 then continue

            # x_error = k_strength * (mag_delta_x - length) * unit_delta_x
            # x1_new = x1 + ((1/m1) / ((1/m1) + (1/m2))) * x_error
            # x2_new = x2 + ((1/m2) / ((1/m1) + (1/m2))) * x_error

            # Error factor.
            vec2.sub pooled_unit_delta, @particle_b.position, @particle_a.position
            mag_delta = vec2.length pooled_unit_delta
            vec2.normalize pooled_unit_delta, pooled_unit_delta
            vec2.scale pooled_error, pooled_unit_delta, @strength
            vec2.scale pooled_error, pooled_error, (mag_delta - @length)

            # Multiply error for mass term.
            vec2.scale pooled_term_a, pooled_error, inv_mass_a
            vec2.scale pooled_term_a, pooled_term_a, 1 / (inv_mass_a + inv_mass_b)
            vec2.scale pooled_term_b, pooled_error, inv_mass_b
            vec2.scale pooled_term_b, pooled_term_b, 1 / (inv_mass_a + inv_mass_b)

            vec2.add @particle_a.position, @particle_a.position, pooled_term_a
            vec2.sub @particle_b.position, @particle_b.position, pooled_term_b

        @_vector_pool.pop()


class Physics.VerletBody extends BaseAABBDaughterShape
    _vector_pool: Util.create_vector_pool 10

    constructor: (@points, @edges, @offset=vec2.create()) ->
        @data = {}
        @collision_class = 0
        @collision_class_ignore = 0
        @collision_ignore = null

        @max_speed = 0

        # @angular_accumulator = 0
        # @angular_acceleration = 0

        # Keep local-space copies of the vertices of the initial verlet particles
        # for transforms, etc
        @local_vertices = []
        for p,i in @points
            @local_vertices.push vec2.clone(p.position)

        # These are world-space attributes generated from the positions of the
        # verlet particles in world-space, or if they have been explictly set by
        # ::set_position, ::set_rotation. last_position is used with position
        # to determine body velocity.
        @position = @_calc_position vec2.create(), @local_vertices
        @last_position = vec2.clone @position
        @velocity = vec2.create()
        @rotation = 0
        @last_rotation = 0
        @angular_velocity = 0

        # Sprites use this position to update themselves.
        @affix_point = @_calc_affix_point vec2.create(), @position, @rotation, @offset

        # Required to satisfy requirements for AABB daughter shape.
        @world_center = @position
        @world_vertices = Array 4
        for i in [0...4]
            @world_vertices[i] = @points[i].position

        # Create AABB only after world_center and world_vertices are defined.
        @aabb = new AABB this

        @_clear_angular_velocity_flag = false

    intersects: (test_body) ->
        if test_body not instanceof Physics.VerletBody
            throw new Error "Physics.VerletBody.intersects() received a non-instance of Physics.VerletBody to test."
        return Intersection.intersects_convex this, test_body

    set_position: (center_vec) ->
        @set center_vec, null

    set_rotation: (angle_rad) ->
        @set null, angle_rad

    # Sets the body's position and rotation while preserving
    # its particles' displacements and angular velocities.
    set: (center_vec=@position, angle_rad=@rotation) ->
        @_vector_pool.push()
        rot_mtx = mat2.create()

        pooled_offset_center = vec2.sub @_vector_pool(), center_vec, @offset
        pooled_v = @_vector_pool()
        mat2.rotate rot_mtx, rot_mtx, -angle_rad

        # Translate particles with the new position and then rotate
        # with the new angle.
        for point, i in @points
            vec2.copy pooled_v, @local_vertices[i]
            vec2.transformMat2 pooled_v, pooled_v, rot_mtx
            vec2.add pooled_v, pooled_v, pooled_offset_center
            point.set_position pooled_v

        @rotation = angle_rad
        @last_rotation = angle_rad
        vec2.copy @position, center_vec
        vec2.copy @last_position, center_vec
        @_calc_affix_point @affix_point, @position, @rotation, @offset
        @_vector_pool.pop()

    displace: (displacement_vec, delta_time=Physics.STEP_TIME) ->
        @_vector_pool.push()
        pooled_v = vec2.negate @_vector_pool(), displacement_vec
        vec2.scale pooled_v, pooled_v, delta_time

        for point in @points
            vec2.add point.last_position, point.last_position, pooled_v

        @_vector_pool.pop()

    # torque: (angle_rad, delta_time=Physics.STEP_TIME) ->
    #     rot_mtx = mat3.create()
    #     mat3.rotate rot_mtx, rot_mtx, -angle_rad*delta_time

    #     for point in @points
    #         # Translate to origin, apply rotation, translate back to where it belongs.
    #         vec2.sub point.last_position, point.last_position, @position
    #         vec2.transformMat3 point.last_position, point.last_position, rot_mtx
    #         vec2.add point.last_position, point.last_position, @position

    clear_displacement: ->
        for point in @points
            point.clear_displacement()

    add_force: (force_vec) ->
        for point in @points
            point.add_force force_vec

    # add_torque: (angle_rad) ->
    #     @angular_accumulator += angle_rad

    clear_forces: ->
        @angular_accumulator = 0
        for point in @points
            point.clear_forces()

    clear_angular_velocity: ->
        @_clear_angular_velocity_flag = true

    integrate: (delta_time) ->
        # # Angular.
        # @angular_acceleration += @angular_accumulator * delta_time
        # this.torque @angular_acceleration
        # @angular_accumulator = 0

        # Linear.
        for point in @points
            point.integrate delta_time

        # Constrain to body shape.
        for edge in @edges
            edge.constrain()

        # Calculate properties.
        @_calc_position @position, @world_vertices
        @_calc_velocity @velocity, @position, @last_position, delta_time
        @rotation = @_calc_rotation @world_vertices, @local_vertices
        @angular_velocity = @_calc_angular_velocity @rotation, @last_rotation
        @_calc_affix_point @affix_point, @position, @rotation, @offset

        # Clear angular velocity.
        # Translate new last-positions of particles from the current displacement.
        if @_clear_angular_velocity_flag
            @_vector_pool.push()
            pooled_d = vec2.sub @_vector_pool(), @position, @last_position
            for point in @points
                vec2.sub point.last_position, point.position, pooled_d
            @_clear_angular_velocity_flag = false
            @_vector_pool.pop()

        @clamp_speed @max_speed

        vec2.copy @last_position, @position
        @last_rotation = @rotation

    # Also, as a necessary consequence, adds drag to the body.
    clamp_speed: (max_speed, delta_time=Physics.STEP_TIME) ->
        if max_speed > 0
            @_vector_pool.push()

            pooled_displacement = vec2.sub @_vector_pool(), @position, @last_position
            s = vec2.length(pooled_displacement) / delta_time

            if s > max_speed
                # Displace the body in the opposite direction by the
                # amount it is over max speed.
                vec2.normalize pooled_displacement, pooled_displacement
                vec2.negate pooled_displacement, pooled_displacement
                vec2.scale pooled_displacement, pooled_displacement, s - max_speed
                @displace pooled_displacement

            @_vector_pool.pop()

    _calc_velocity: (out, v, v_last, delta_time=Physics.STEP_TIME) ->
        vec2.sub out, v, v_last
        vec2.scale out, out, 1.0/delta_time
        return out

    _calc_angular_velocity: (t, t_last, delta_time=Physics.STEP_TIME) ->
        # This method is HELLZA INACCURATE -- it will br0ke if the
        # angular velocity is greater than PI. TNP!
        w = Util.map_to_range(t - t_last) * 1.0/delta_time
        return w

    _calc_affix_point: (out, center, rotation, offset=vec2.create()) ->
        m = mat3.create()
        vec2.copy out, offset

        mat3.translate m, m, center
        mat3.rotate m, m, rotation
        vec2.transformMat3 out, out, m
        return out

    # Algebraic center of vertices.
    _calc_position: (out, vertices) ->
        @_vector_pool.push()
        pooled_tl = @_vector_pool()
        pooled_br = @_vector_pool()

        pooled_tl[0] = pooled_tl[1] = Number.MAX_VALUE
        pooled_br[0] = pooled_br[1] = -Number.MAX_VALUE

        for v in vertices
            if v[0] < pooled_tl[0] then pooled_tl[0] = v[0]
            if v[0] > pooled_br[0] then pooled_br[0] = v[0]
            if v[1] < pooled_tl[1] then pooled_tl[1] = v[1]
            if v[1] > pooled_br[1] then pooled_br[1] = v[1]

        width = pooled_br[0] - pooled_tl[0]
        height = pooled_br[1] - pooled_tl[1]

        vec2.set out, (width*0.5)+pooled_tl[0], (height*0.5)+pooled_tl[1]

        @_vector_pool.pop()
        return out

    _calc_rotation: (world, local) ->
        @_vector_pool.push()
        # Calculate difference in angle between a pair of sample edges
        # from the the local, untransformed and (world-space) transformed vertices.
        pooled_v1 = vec2.sub @_vector_pool(), local[1], local[0]
        pooled_v2 = vec2.sub @_vector_pool(), world[1], world[0]
        vec2.normalize pooled_v2, pooled_v2
        vec2.normalize pooled_v1, pooled_v1

        theta = Math.acos(vec2.dot(pooled_v1, pooled_v2))

        # So we get a full circular angle between 0-2PI instead of the most
        # acute angle (0-PI).
        v = vec2.cross vec3.create(), pooled_v1, pooled_v2
        if v[2] <= 0
            theta = (Math.PI*2) - theta

        @_vector_pool.pop()
        return theta

    # Center of mass.
    _calc_centroid: (out, points) ->
        @_vector_pool.push()
        pooled_vector_sum = @_vector_pool()
        pooled_vector_mass = @_vector_pool()

        total_mass = 0.0

        # Sum point masses as vectors.
        for point in points
            vec2.scale pooled_vector_mass, point.position, point.get_mass()
            vec2.add pooled_vector_sum, pooled_vector_sum, pooled_vector_mass
            total_mass += point.get_mass()

        # Divide by total mass
        vec2.scale out, pooled_vector_sum, 1.0/total_mass

        @_vector_pool.pop()
        return out

    draw_debug: (context, transform) ->
        if DEBUG_DRAW_BOUNDS
            @aabb.draw_debug context, transform


class Physics.VerletRectBody extends Physics.VerletBody
    constructor: (dim, @mass = 1.0) ->
        dim.cx = dim.cx or 0
        dim.cy = dim.cy or 0
        dim.height = dim.height or 1
        dim.width = dim.width or 1
        dim.halfwidth = dim.width * 0.5
        dim.halfheight = dim.height * 0.5
        point_mass = @mass * 0.25

        points = Array 4
        points[0] = new Physics.VerletParticle -dim.halfwidth, -dim.halfheight, point_mass
        points[1] = new Physics.VerletParticle -dim.halfwidth, dim.halfheight, point_mass
        points[2] = new Physics.VerletParticle dim.halfwidth, -dim.halfheight, point_mass
        points[3] = new Physics.VerletParticle dim.halfwidth, dim.halfheight, point_mass

        edges = Array 6
        # Box shape
        edges[0] = new Physics.VerletConstraint points[0], points[1]
        edges[1] = new Physics.VerletConstraint points[1], points[2]
        edges[2] = new Physics.VerletConstraint points[2], points[3]
        edges[3] = new Physics.VerletConstraint points[3], points[0]
        # Diagonals
        edges[4] = new Physics.VerletConstraint points[0], points[2]
        edges[5] = new Physics.VerletConstraint points[1], points[3]

        offset = vec2.fromValues -dim.cx, -dim.cy
        super points, edges, offset

    draw_debug: (context, transform) ->
        super context, transform

        if DEBUG_DRAW_BODIES
            context.strokeStyle = "brown"
            context.fillStyle = "brown"
            pa = vec2.create()
            pb = vec2.create()

            for edge in @edges
                vec2.copy pa, edge.particle_a.position
                vec2.copy pb, edge.particle_b.position
                vec2.transformMat3 pa, pa, transform
                vec2.transformMat3 pb, pb, transform

                context.beginPath()
                context.lineTo pa[0], pa[1]
                context.lineTo pb[0], pb[1]
                context.stroke()

            for point in @points
                vec2.copy pa, point.position
                vec2.transformMat3 pa, pa, transform

                context.beginPath()
                context.arc pa[0], pa[1], 2.2, 0, Math.PI * 2
                context.fill()

        # context.beginFill("brown")
        # context.drawCircle @affix_point[0], @affix_point[1], 0.3
        # context.endFill()

        # context.beginFill("blue")
        # context.drawCircle @affix_point[0], @affix_point[1], 0.3
        # context.endFill()

        # if @manifold?
        #     c = @manifold.contacts[0]
        #     context.beginStroke("green")
        #     context.lineTo c.incident_ref[0][0], c.incident_ref[0][1]
        #     context.lineTo c.incident_ref[1][0], c.incident_ref[1][1]
        #     context.endStroke()

        #     context.beginFill("green")
        #     context.drawCircle c.vertex_ref[0], c.vertex_ref[1], 0.2
        #     context.endFill()

        #     separation = vec2.scale vec2.create(), c.normal, c.depth
        #     context.beginStroke("green")
        #     context.lineTo c.vertex_ref[0], c.vertex_ref[1]
        #     context.lineTo c.vertex_ref[0]+separation[0], c.vertex_ref[1]+separation[1]
        #     context.endStroke()


class BodyCollisionEvent extends Event
    event_name: "body_collision_event"
    constructor: (@body, @collider, @manifold) ->
