#_require util.coffee


EPA_TOLERANCE = 0.00001
RELATIVE_TOL = 0.98
ABSOLUTE_TOL = 0.001
MAX_MANIFOLD_POINTS = 2


class ContactManifold
    constructor: ->
        @contacts = Array 2
        @body_a = null
        @body_b = null


class Contact
    constructor: () ->
        @position = vec2.create()
        @vertex_ref = null
        @incident_ref = null
        @normal = vec2.create()
        @depth = 0
        @flip = false
        @reference = null


Intersection = {}
Collision = {}


do ->
    _vector_pool = Util.create_vector_pool 30

    Intersection.intersects_aabb = (center_a, width_a, height_a, center_b, width_b, height_b) ->
        x_test = Math.abs(center_a[0] - center_b[0]) * 2 < (width_a + width_b)
        y_test = Math.abs(center_a[1] - center_b[1]) * 2 < (height_a + height_b)
        return (x_test and y_test)

    Intersection.encloses_aabb = (center_a, width_a, height_a, center_b, width_b, height_b) ->
        x_test = Math.abs(center_a[0] - center_b[0]) * 2 < (width_a - width_b)
        y_test = Math.abs(center_a[1] - center_b[1]) * 2 < (height_a - height_b)
        return (x_test and y_test)

    # Determine accurate intersection via the Gilbert-Johnson-Keerthi distance algorithm.
    Intersection.intersects_convex = (body_a, body_b) ->
        _vector_pool.push()

        is_same_direction = (v1, v2) ->
            return (vec2.dot v1, v2) > 0.0

        vertices_a = _get_body_vertices body_a
        vertices_b = _get_body_vertices body_b

        # Intermediaries used for GJK solver loop.
        pooled_AB = _vector_pool()
        pooled_AC = _vector_pool()
        pooled_AO = _vector_pool()
        pooled_ACB = _vector_pool()
        pooled_ABC = _vector_pool()
        pooled_neg_AC = _vector_pool()
        pooled_neg_AB = _vector_pool()

        pooled_next_point = _vector_pool()
        pooled_search_direction = _vector_pool 1.0, 0.0

        # Statically creating a simplex array instead of dynamic insertions/deletions
        # to prevent unnecessary garbage for the GC. More annoying, but more effecient.
        # Note these vectors are not from the pool because the function will return
        # the simplex on an affirmative result, so we need fresh new instances.
        simplex = [vec2.create(), vec2.create(), vec2.create()]
        simplex_length = 0

        # Prime 0-simplex.
        _support pooled_next_point, vertices_a, vertices_b, pooled_search_direction
        vec2.copy simplex[0], pooled_next_point
        simplex_length = 1

        # Next search direction for simplex point 2.
        vec2.negate pooled_search_direction, simplex[0]

        # Adapted from from http:#entropyinteractive.com/2011/04/gjk-algorithm/
        while true
            _support pooled_next_point, vertices_a, vertices_b, pooled_search_direction

            # Does not pass the origin AO, cannot collide.
            if not is_same_direction pooled_next_point, pooled_search_direction
                _vector_pool.pop()
                return false

            # Insert new simplex point.
            vec2.copy simplex[simplex_length], pooled_next_point
            simplex_length += 1

            switch simplex_length
                # 0-simplex -- we should actually never have this state.
                when 1
                    continue

                # 1-simplex
                when 2
                    sa = simplex[1]
                    sb = simplex[0]
                    vec2.sub pooled_AB, sb, sa
                    vec2.set pooled_AO, -sa[0], -sa[1]

                    if is_same_direction pooled_AO, pooled_AB
                        # Perpendicular vector to pooled_AB
                        vec2.set pooled_search_direction, pooled_AB[1], -pooled_AB[0]
                        vec2.scale pooled_search_direction, pooled_search_direction, -vec2.dot(sa, pooled_search_direction)
                    else
                        vec2.negate pooled_search_direction, sa
                        # simplex = [sa]
                        simplex_length = 1
                        vec2.copy simplex[0], sa

                # 2-simplex
                when 3
                    sa = simplex[2]
                    sb = simplex[1]
                    sc = simplex[0]
                    vec2.sub pooled_AB, sb, sa
                    vec2.sub pooled_AC, sc, sa
                    vec2.set pooled_AO, -sa[0], -sa[1]

                    # Perpendicular vector to AB facing away from C
                    vec2.set pooled_ACB, pooled_AB[1], -pooled_AB[0]
                    vec2.negate pooled_neg_AC, pooled_AC
                    vec2.scale pooled_ACB, pooled_ACB, vec2.dot(pooled_ACB, pooled_neg_AC)

                    # Perpendicular vector to AC facing away from B
                    vec2.set pooled_ABC, pooled_AC[1], -pooled_AC[0]
                    vec2.negate pooled_neg_AB, pooled_AB
                    vec2.scale pooled_ABC, pooled_ABC, vec2.dot(pooled_ABC, pooled_neg_AB)

                    if is_same_direction pooled_ACB, pooled_AO
                        # Region 4
                        if is_same_direction pooled_AB, pooled_AO
                            pooled_search_direction = pooled_ACB
                            # simplex = [sa, sb]
                            simplex_length = 2
                            # vec2.copy simplex[1], sb # sb=simplex[1], redundant
                            vec2.copy simplex[0], sa
                        # Region 5
                        else
                            pooled_search_direction = pooled_AO
                            # simplex = [sa]
                            simplex_length = 1
                            vec2.copy simplex[0], sa

                    else if is_same_direction pooled_ABC, pooled_AO
                        # Region 6
                        if is_same_direction pooled_AC, pooled_AO
                            pooled_search_direction = pooled_ABC
                            # simplex = [sa, sc]
                            simplex_length = 2
                            vec2.copy simplex[1], sc # sc=simplex[0], so must copy before simplex[0]<-sa
                            vec2.copy simplex[0], sa
                        # Region 5
                        else
                            pooled_search_direction = pooled_AO
                            # simplex = [sa]
                            simplex_length = 1
                            vec2.copy simplex[0], sa

                    # Region 7, simplex covers pooled_AO
                    else
                        _vector_pool.pop()
                        return simplex

    Collision.create_manifold = (body_a, body_b, intersecting_simplex=null) ->
        c = Collision.sat body_a, body_b

        if c
            manifold = new ContactManifold()
            manifold.body_a = body_a
            manifold.body_b = body_b
            manifold.contacts = [c]
            return manifold
        else
            return null

    # Determine penetration depth and normal after a collision using
    # Expanding Polytope Algorithm.
    Collision.epa_penetration = (out, body_a, body_b, intersecting_simplex) ->
        _vector_pool.push()
        closest_edge = {
            distance: 0,
            pooled_normal: _vector_pool(),
            index: 0
        }

        find_closest_edge = (shape) ->
            _vector_pool.push()

            closest_edge.distance = Number.MAX_VALUE
            pooled_n = _vector_pool()
            pooled_edge = _vector_pool()

            for vertex, i in shape
                j = (i + 1) % shape.length

                a = vertex
                b = shape[j]
                origin = a # b also works
                vec2.sub pooled_edge, b, a

                # Edge to origin (perpendicular).
                vec2.set pooled_n, -edge[1], edge[0]
                vec2.normalize pooled_n, pooled_n

                # Distance from origin to normal.
                distance = vec2.dot origin, pooled_n

                if distance < closest_edge.distance
                    vec2.copy closest_edge.pooled_normal, pooled_n
                    closest_edge.distance = distance
                    closest_edge.index = j

            _vector_pool.pop()
            return closest_edge

        vertices_a = _get_body_vertices body_a
        vertices_b = _get_body_vertices body_b
        shape = intersecting_simplex

        edge = null
        distance = 0

        while true
            edge = find_closest_edge shape

            p = _support vec2.create(), vertices_a, vertices_b, edge.pooled_normal
            distance = vec2.dot p, edge.normal

            # New support point was not closer than the closest
            # edge (within tolerance), so we cannot expand the shape
            # further.
            if Math.abs(distance - edge.distance) < EPA_TOLERANCE
                break

            # Insert the point at the closest edge index.
            else
                shape.splice edge.index, 0, p

        _vector_pool.pop()
        return vec2.scale out, edge.pooled_normal, distance

    # Separating Axis Theorem test -- does both collision detection and contact resolution!
    # Only returns a single contact point, though.
    Collision.sat = (body_a, body_b, collision_only=false) ->
        _vector_pool.push()

        get_edge_points = (vertices, edge_idx) ->
            e = [vertices[(edge_idx + 1) % vertices.length], vertices[edge_idx]]
            e.parent = vertices
            return e

        sign = (A) ->
            return (if A < 0 then -1 else 1)

        project_to_axis = (axis, vertices) ->
            dot = vec2.dot axis, vertices[0]
            min = max = dot

            for vertex in vertices
                # Project the rest of the vertices onto the axis and extend the interval to the left/right if necessary.
                dot = vec2.dot axis, vertex
                min = Math.min(dot, min)
                max = Math.max(dot, max)

            return [min, max]

        interval_distance = (min_a, max_a, min_b, max_b) ->
            if min_a < min_b
                return min_b - max_a
            else
                return min_a - max_b

        vertices_a = _get_body_vertices body_a
        vertices_b = _get_body_vertices body_b

        contact_normal = vec2.create()
        contact_vertex_ref = null
        incident_ref = null
        depth = 0
        flip = false

        # Used for SAT solving loop
        pooled_edge_dist = _vector_pool()
        pooled_axis = _vector_pool()
        min_distance = Number.MAX_VALUE

        # Iterate through all the edges of both bodies and find if there are any
        # separating axes. Along the way we can collect possible incident edges and
        # contact normals.
        for i in [0...(vertices_a.length + vertices_b.length)]
            if i < vertices_a.length
                edge = get_edge_points(vertices_a, i)
            else
                edge = get_edge_points(vertices_b, i - vertices_a.length)

            vec2.sub pooled_edge_dist, edge[1], edge[0]

            # Perpendicular to edge.
            vec2.set pooled_axis, -pooled_edge_dist[1], pooled_edge_dist[0]
            vec2.normalize pooled_axis, pooled_axis

            # Project both bodies onto the perpendicular.
            [min_a, max_a] = project_to_axis pooled_axis, vertices_a
            [min_b, max_b] = project_to_axis pooled_axis, vertices_b

            # Distance between the two intervals.
            distance = interval_distance min_a, max_a, min_b, max_b

            # Cannot be colliding if just one pair of intervals are not overlapping.
            if distance > 0.00001
                _vector_pool.pop()
                return false

            else if Math.abs(distance) < min_distance
                min_distance = Math.abs distance
                vec2.copy contact_normal, pooled_axis
                incident_ref = edge

        depth = min_distance

        if collision_only
            _vector_pool.pop()
            return true

        # Collision occured, find the contact point.

        # body1 contains the contact point and body2 contains the incident edge.
        if incident_ref.parent == vertices_b
            body1_vertices = vertices_a
            body2_vertices = vertices_b
            flip = false
        else
            body1_vertices = vertices_b
            body2_vertices = vertices_a
            flip = true

        centroid1 = _get_centroid body1_vertices
        centroid2 = _get_centroid body2_vertices

        # Ensure contact normal is in the direction of body 1.
        if sign(vec2.dot(contact_normal, vec2.sub(_vector_pool(), centroid1, centroid2)))  != 1
            vec2.negate contact_normal, contact_normal

        # Find contact vertex -- the vertex of body1 closest to body2.
        t = _vector_pool()
        smallest_dist = Number.MAX_VALUE
        for vertex in body1_vertices
            distance = vec2.dot contact_normal, vec2.sub(t, vertex, centroid1)
            if distance < smallest_dist
                smallest_dist = distance
                contact_vertex_ref = vertex

        contact = new Contact()
        contact.vertex_ref = contact_vertex_ref
        contact.position = vec2.clone contact_vertex_ref
        contact.incident_ref = incident_ref
        contact.normal = contact_normal
        contact.depth = depth
        contact.flip = flip

        _vector_pool.pop()
        return contact

    # Find contact point(s) via polygon clipping.
    Collision.find_contact_points = (body_a, body_b, penetrate_vector=null) ->
        _vector_pool.push()

        get_edge_points = (vertices, edge_idx) ->
            return [vertices[(edge_idx + 1) % vertices.length], vertices[edge_idx]]

        # Sutherland–Hodgman polygon clipping.
        clip_line_segment = (v1, v2, normal, offset) ->
            d1 = vec2.dot(normal, v1) - offset
            d2 = vec2.dot(normal, v2) - offset
            clipped = []

            # Next two conditionals are inside the clipping area.
            if d1 >= 0
                clipped.push v1

            if d2 >= 0
                clipped.push v2

            # Past clipping area, clip point to line.
            if d1 * d2 < 0
                e = vec2.sub vec2.create(), v2, v1
                vec2.scale e, e, d1/(d1-d2)
                vec2.add e, e, v1
                clipped.push e

            return clipped

        vertices_a = _get_body_vertices body_a
        vertices_b = _get_body_vertices body_b
        contacts = []

        [edge_a_idx, separation_a] = Collision.find_max_separation body_a, body_b
        [edge_b_idx, separation_b] = Collision.find_max_separation body_b, body_a

        if separation_b > (RELATIVE_TOL * separation_a + ABSOLUTE_TOL)
            ref = get_edge_points vertices_b, edge_b_idx
            inc = get_edge_points vertices_a, edge_a_idx
            ref_edge_idx = edge_b_idx
            flip = true
        else
            ref = get_edge_points vertices_a, edge_a_idx
            inc = get_edge_points vertices_b, edge_b_idx
            ref_edge_idx = edge_a_idx
            flip = false

        pooled_ref_n = vec2.normalize _vector_pool(), vec2.sub(_vector_pool(), ref[1], ref[0])

        o1 = vec2.dot pooled_ref_n, ref[0]
        cp = clip_line_segment inc[1], inc[0], pooled_ref_n, o1
        if cp.length < 2
            _vector_pool.pop()
            return false

        o2 = -vec2.dot(pooled_ref_n, ref[1])
        cp = clip_line_segment cp[1], cp[0], vec2.negate(_vector_pool(), pooled_ref_n), o2
        if cp.length < 2
            _vector_pool.pop()
            return flse

        pooled_ref_perp = _vector_pool -pooled_ref_n[1], pooled_ref_n[0]
        # if flip
            # vec2.negate pooled_ref_perp, pooled_ref_perp

        max = vec2.dot pooled_ref_perp, ref[0]
        depth0 = vec2.dot(pooled_ref_perp, cp[0]) - max
        depth1 = vec2.dot(pooled_ref_perp, cp[1]) - max

        if depth0 > 0.0
            c = new Contact()
            c.position = vec2.clone cp[0]
            c.incident_ref = inc
            c.reference = ref
            c.depth = -depth0
            c.normal = if penetrate_vector? then (vec2.normalize vec2.create(), penetrate_vector) else null
            c.flip = flip
            contacts.push c
            # contacts.push new Contact(vec2.clone cp[0], penetrate_normal, -depth0)

        if depth1 > 0.0
            c = new Contact()
            c.position = vec2.clone cp[1]
            c.incident_ref = inc
            c.reference = ref
            c.depth = -depth1
            c.normal = if penetrate_vector? then (vec2.normalize vec2.create(), penetrate_vector) else null
            c.flip = flip
            contacts.push c
            # contacts.push new Contact(vec2.clone cp[1], penetrate_normal, -depth1)

        _vector_pool.pop()
        return contacts

    Collision.find_max_separation = (body_a, body_b) ->
        _vector_pool.push()

        edge_separation = (body_a, body_b, edge_idx) ->
            vertices_a = _get_body_vertices body_a
            vertices_b = _get_body_vertices body_b
            normals_a = _gen_normals vertices_a

            # assert(0 <= edge_idx and edge_idx < vertices_a.length)
            normal_a = normals_a[edge_idx]

            # Find support vertex on body_b for -normal.
            support_idx = _closest_point_index vertices_b, normal_a

            v1 = vertices_a[edge_idx]
            v2 = vertices_b[support_idx]
            separation = vec2.dot vec2.sub(_vector_pool(), v2, v1), normal_a

            return separation

        vertices_a = _get_body_vertices body_a
        vertices_b = _get_body_vertices body_b
        normals_a = _gen_normals vertices_a

        # Vector pointing from the centroid of body_a to the centroid of body_b.
        pooled_d = vec2.sub _vector_pool(), _get_centroid(vertices_b), _get_centroid(vertices_a)

        # Find edge normal on body_a that has the largest projection onto d.
        edge_idx = _furthest_point_index normals_a, pooled_d

        # Get the separation for the edge normal.
        sep = edge_separation body_a, body_b, edge_idx

        # Check the separation for the previous edge normal.
        prev_edge = (edge_idx + vertices_a.length - 1) % vertices_a.length
        sep_prev = edge_separation body_a, body_b, prev_edge

        # Check the separation for the next edge normal.
        next_edge = (edge_idx + 1) % vertices_a.length
        sep_next = edge_separation body_a, body_b, next_edge

        # Find the best edge (greatest separation) and the search direction.
        # if sep_prev > sep and sep_prev > sep_next
        if sep < sep_prev > sep_next
            increment = -1
            best_edge = prev_edge
            best_separation = sep_prev
        else if sep_next > sep
            increment = 1
            best_edge = next_edge
            best_separation = sep_next
        else
            _vector_pool.pop()
            return [edge_idx, sep]

        # Perform a local search for the best edge normal.
        while true
            if increment == -1
                edge_idx = (best_edge + vertices_a.length - 1) % vertices_a.length
            else
                edge_idx = (best_edge + 1) % vertices_a.length

            sep = edge_separation body_a, body_b, edge_idx

            if sep > best_separation
                best_edge = edge_idx
                best_separation = sep
            else
                break

        _vector_pool.pop()
        return [best_edge, best_separation]

    _get_body_vertices = (body) ->
        if "world_vertices" of body then body.world_vertices else body

    # Return furthest vertex of a Minkowski Sum's shape along a direction,
    # used by GJK and EPA algorithms.
    # p = max(d·a) - max(-d·b)
    _support = (out, vertices_a, vertices_b, direction) ->
        neg_direction = vec2.negate vec2.create(), direction
        vec2.sub out, _furthest_point(vertices_a, direction), _furthest_point(vertices_b, neg_direction)
        return out

    _furthest_point = (vector_array, direction) ->
        i = _furthest_point_index vector_array, direction
        return vector_array[i]

    _furthest_point_index = (vector_array, direction) ->
        max = -Number.MAX_VALUE
        furthest = 0

        # Furthest point is the one with the least
        # difference in angle against the direction
        # (i.e. the greatest projection on the
        # the direction)
        for point, index in vector_array
            dot = vec2.dot point, direction
            if dot > max
                max = dot
                furthest = index

        return furthest

    _closest_point = (vector_array, direction) ->
        i = _closest_point_index vector_array, direction
        return vector_array[i]

    _closest_point_index = (vector_array, direction) ->
        min = Number.MAX_VALUE
        closest = 0

        # Most difference, least projection.
        for point, index in vector_array
            dot = vec2.dot point, direction
            if dot < min
                min = dot
                closest = index

        return closest

    # This should be a temporary clutch... generating normals for each
    # polygon for each collision test is highly unadvised.
    _gen_normals = (vertices) ->
        normals = []
        for i in [0...vertices.length]
            v1 = vertices[i]
            v2 = vertices[(i+1) % vertices.length]
            n = vec2.sub vec2.create(), v2, v1
            vec2.set n, -n[1], n[0]
            vec2.normalize n,n
            normals.push n

        return normals

    # Also temporary!
    # Assuming quadrilateral and taking midpoint of diagonals for now...
    _get_centroid = (vertices) ->
        c = vec2.sub vec2.create(), vertices[2], vertices[0]
        vec2.scale c, c, 0.5
        vec2.add c, vertices[0], c
        return c
