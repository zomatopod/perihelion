#_require conf


EventDispatcher = {}


class EventDispatcherError extends Error
    name: "EventDispatcherError"
    constructor: (@message) ->


do ->
    _listeners = {}                     # listeners[event_name_key] = [callback1, callback2, ...]
    _event_queue = []                   # event_queue = [event_obj1, event_obj2, ...]
    _remove_listener_queue = []         # _remove_listener_queue = [[event_obj1, callback1], [event_obj1, callback2], ...]

    EventDispatcher.add_listener = (event, callback) ->
        event_obj = _create_event_obj event

        if not callback?
            throw new EventDispatcherError "Undefined callback."

        # Create callback array for listener class if it doesn't exist (first reg).
        if event_obj.event_name not of _listeners
            _listeners[event_obj.event_name] = []

        if _listeners[event_obj.event_name].indexOf(callback) > -1
            throw new EventDispatcherError "Callback has already been registered for event '#{event_obj.event_name}':\n#{callback}"

        _listeners[event_obj.event_name].push callback

    # Listeners are queued for removal so that they won't be removed in the middle
    # of the dispatch loop, which will cause the dispatcher to send events to
    # now-invalid callbacks.
    EventDispatcher.remove_listener = (event, callback) ->
        event_obj = _create_event_obj event
        _remove_listener_queue.push [event_obj, callback]

    EventDispatcher.remove_all_listeners = (event) ->
        event_obj = _create_event_obj event
        if event_obj.event_name of _listeners
            for callback in _listeners[event_obj.event_name]
                _remove_listener_queue.push [event_obj, callback]

    EventDispatcher.queue_event = (event_obj) ->
        # Assert event_obj is proper.
        event_obj = _create_event_obj event_obj
        _event_queue.push event_obj

    EventDispatcher.clear_events = ->
        _cull_dead_listeners()
        _event_queue.length = 0

    EventDispatcher.dispatch = ->
        _cull_dead_listeners()

        # Swap events to empty processing queue. New events created
        # during dispatch wlll be buffered to the next call.
        proc_queue = []
        [proc_queue, _event_queue] = [_event_queue, proc_queue]

        for event_obj in proc_queue
            _fire_event event_obj

    EventDispatcher.trigger = (event_obj) ->
        _cull_dead_listeners()
        _fire_event event_obj

    _fire_event = (event_obj) ->
        # Assert event_obj is proper.
        event_obj = _create_event_obj event_obj
        if event_obj.event_name of _listeners
            for callback in _listeners[event_obj.event_name]
                callback(event_obj)

    _cull_dead_listeners = ->
        for [event_obj, callback] in _remove_listener_queue
            if event_obj.event_name of _listeners
                index = _listeners[event_obj.event_name].indexOf callback
                if index > -1
                    _listeners[event_obj.event_name].splice index, 1

        _remove_listener_queue.length = 0

    _create_event_obj = (event) ->
        # Instantiated objects or class constructor function.
        if typeof event == "object" or typeof event == "function"
            if event.event_name?
                event_obj = event
            else if event::event_name?
                event_obj = event
                event_obj.event_name = event.prototype.event_name
            else
                throw new EventDispatcherError "Event object argument does not have an event_name property."

        # All other types will use their own values as the event name.
        else
            event_obj = {event_name: event}

        return event_obj


class EventListenable
    add_listener: (event, callback) ->
        if not @_listeners? then @_listeners = []
        EventDispatcher.add_listener event, callback
        @_listeners.push [event, callback]

    __destroy_listeners: ->
        if @_listeners then for [event, callback] in @_listeners
            EventDispatcher.remove_listener event, callback


class Event
    event_name: "default_event"
    constructor: ->


class TickEvent
    event_name: "tick"
    constructor: (@delta_time) ->