Sound = {}
Sound.falloff_functions = {}


Sound.falloff_functions.linear = (distance, falloff_ref=0, falloff_max=Number.MAX_VALUE, falloff_factor=1.0) ->
    v = 1.0 - (falloff_factor * ((distance - falloff_ref) / (falloff_max - falloff_ref)))
    return Math.max(0.0, Math.min(1.0, v))


do ->
    _sound_listeners = []

    Sound.add_sound_listener = (listener) ->
        _sound_listeners.push listener

    Sound.remove_sound_listener = (listener) ->
        index = _sound_listeners.indexOf listener
        _sound_listeners.splice(index, 1) if index > -1


    class Sound.Emitter
        constructor: (options={}) ->
            @falloff_max = options.falloff_max or 75
            @falloff_ref = options.falloff_ref or 10
            @falloff_factor = options.falloff_factor or 1.0
            @max_distance = options.max_distance or Number.MAX_VALUE
            @falloff_function = options.falloff_function or Sound.falloff_functions.linear

            @loops = []

        _play: (id=null, source_position=null, looping=0) ->
            sounds = []

            if id?
                for listener in _sound_listeners
                    volume = this.get_volume(source_position, listener.get_sound_listener_position())

                    if DEBUG
                        sound = createjs.Sound.play(id, null, 0, 0, looping, volume, 0)
                        throw new Error("Could not play sound #{id}.") if sound.playState == createjs.Sound.PLAY_FAILED

                        sounds.push sound
                        continue

                    sounds.push createjs.Sound.play(id, null, 0, 0, looping, volume, 0)

            return sounds

        emit: (id=null, source_position=null) ->
            this._play id, source_position, 0

        loop: (id=null, source_position=null) ->
            if id?
                @loops = @loops.concat this._play(id, source_position, -1)

        update_loops: (source_position=null) ->
            for listener in _sound_listeners
                for sound in @loops
                    sound.setVolume this.get_volume(source_position, listener.get_sound_listener_position())

        stop_loops: ->
            sound.stop() for sound in @loops
            @loops.length = 0

        get_volume: (source_position=null, listener_position=null) ->
            if not source_position? or not listener_position? then return 1.0

            distance = vec2.length vec2.sub(vec2.create(), source_position, listener_position)
            distance = Math.min distance, @max_distance
            return @falloff_function(distance, @falloff_ref, @falloff_max, @falloff_factor)


    _default_emitter = new Sound.Emitter()

    Sound.emit = (id, source_position) ->
        _default_emitter.emit id, source_position

    Sound.play_loop = (id) ->
        _default_emitter.loop id


class SoundListener
    get_sound_listener_position: ->
        throw new Error "Abstract SoundListener::get_sound_listener_position() must be overridden."

    add_sound_listener: ->
        Sound.add_sound_listener this

    __destroy_sound_listener: ->
        Sound.remove_sound_listener this

