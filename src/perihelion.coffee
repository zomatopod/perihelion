# glMatrix, in fine OpenGL tradition, uses column vectors stored in column-major
# order in its array (or row vectors stored in row-major order, if you prefer):
# [1, 0, 0, 0,
#  0, 1, 0, 0,
#  0, 0, 1, 0,
#  x, y, z, 0]
#
# http://steve.hollasch.net/cgindex/math/matrix/column-vec.html
#
# PIXI.js's local and world transform matrices are transposed of glMatrix's.


#_require conf
#_require base
#_require assets
#_require input
#_require event
#_require objects
#_require actor_player
#_require actor_enemies
#_require collision
#_require sound
#_require util

if DEBUG
    console.log "Running in debug mode."


class PHObject extends mixOf Destructable, EventListenable
    null


class window.Perihelion
    constructor: (@render_canvas, @debug_canvas=null) ->
        # Disable context menu on canvases.
        @render_canvas.oncontextmenu = ->
            return false

        @debug_canvas?.oncontextmenu = ->
            return false

        @render_canvas.width = RENDER_WIDTH
        @render_canvas.height = RENDER_HEIGHT

        # Debug canvas's coordinate space must match the internal rendering
        # resolution or else its output will be clipped.
        if @debug_canvas
            @debug_canvas.width = RENDER_WIDTH
            @debug_canvas.height = RENDER_HEIGHT

        # Duplicate render canvas to display loading screen.
        # (Loading canvas will use 2D context and render canvas
        # may use WebGL, and contexts are permanent once created.)
        @load_canvas = @render_canvas.cloneNode()
        @render_canvas.parentNode.appendChild @load_canvas

        AssetManager.load_manifest_file ASSET_MANIFEST, {
            progress: (percent) =>
                load_context = @load_canvas.getContext('2d')
                load_context.font = "200px 'Titillium Web', sans-serif"
                load_context.clearRect 0, 0, RENDER_WIDTH, RENDER_HEIGHT

                text = Math.floor(percent * 100) + "%"
                text_width = load_context.measureText(text).width

                load_context.fillStyle = "white"
                load_context.fillText text, (@load_canvas.width * 0.5) - (text_width * 0.5), @load_canvas.height  * 0.5

            failure: (error) =>
                load_context = @load_canvas.getContext('2d')
                load_context.font = "100px 'Titillium Web', sans-serif"
                load_context.clearRect 0, 0, RENDER_WIDTH, RENDER_HEIGHT

                text = "LOADING ERROR"
                text_width = load_context.measureText(text).width

                load_context.fillStyle = "red"
                load_context.fillText text, (@load_canvas.width * 0.5) - (text_width * 0.5), @load_canvas.height  * 0.5

            success: =>
                @load_canvas.parentNode.removeChild @load_canvas
                delete @load_canvas

                this.start_game()
        }

    start_game: =>
        @renderer = PIXI.autoDetectRenderer RENDER_WIDTH, RENDER_HEIGHT, @render_canvas

        @stage = new PerihelionStage 100, 56, @render_canvas
        @world = new PerihelionWorld @stage, 250, 250
        @tick_event = new TickEvent(0)
        @timer = new Util.Timer()

        # Setup some debug keys.
        if DEBUG
            EventDispatcher.add_listener InputKeydownEvent, (event) ->
                switch event.keycode
                    when Input.keycodes.key_f8
                        DEBUG_DRAW_BOUNDS = DEBUG_DRAW_BODIES = not DEBUG_DRAW_BOUNDS
                    when Input.keycodes.key_f7
                        DEBUG_DRAW_QUADTREE = not DEBUG_DRAW_QUADTREE

        Util.request_animation_frame this.update

    update: =>
        Util.request_animation_frame this.update
        @timer.update()
        @tick_event.delta_time = @timer.delta

        Input.queue_input_events()
        EventDispatcher.dispatch()
        EventDispatcher.trigger @tick_event

        @stage.render_postfx()
        @renderer.render @stage

        if DEBUG
            if @debug_canvas
                context = @debug_canvas.getContext('2d')
                context.clearRect 0, 0, RENDER_WIDTH, RENDER_HEIGHT
                @world.draw_debug context


class PerihelionStage extends mixOf PIXI.Stage, PHObject
    constructor: (@viewport_width, @viewport_height, @canvas_element) ->
        # PIXI.js uses non-prototype constructor() to initialize, which
        # unfortunately clashes with CS class constructors,
        # so never use @constructor(...) or else you'll get infinite
        # recursion.
        PIXI.Stage.constructor.call(this, 0xffffff, false)

        this.add_listener InputMousemoveEvent, @mousemove_callback
        this.add_listener TickEvent, @update

        @mouse_position = vec2.create()

        @render_width = @canvas_element.width
        @render_height = @canvas_element.height

        # TODO: These will become invalidated if the canvas ever changes
        # mid-game (which shouldn't happen but who knows).
        # Register a callback to update them?
        @canvas_css_width  = parseInt(Util.get_css(@canvas_element, "width"))
        @canvas_css_height = parseInt(Util.get_css(@canvas_element, "height"))

        this.reset()

    reset: ->
        if @postfx? then for [fx, fx_sprite] in @postfx
            fx.destroy()

        while @children.length > 0
            this.removeChild @children[0]

        @layers = new ObjectLayerCollection this
        @viewport = new PIXI.DisplayObjectContainer()
        @postfx = []

        # Size/position viewport to renderer resolution.
        this._set_viewport_to_resolution()
        this.addChild @viewport

    __destroy_stage: ->
        while @children.length > 0
            this.removeChild @children[0]

    _set_viewport_to_resolution: (res_width=@render_width, res_height=@render_height) ->
        x_factor = res_width / @viewport_width
        y_factor = res_height / @viewport_height

        # Scale viewport's logical size to the resolution.
        @viewport.scale.x = x_factor
        @viewport.scale.y = y_factor

        # Place (0,0) origin in the middle.
        @viewport.position.x = (@viewport_width * 0.5) * x_factor
        @viewport.position.y = (@viewport_height * 0.5) * y_factor

        return @viewport

    new_postfx: (postfx_class) ->
        res = postfx_class.prototype.resolution
        num = postfx_class.prototype.buffers

        fx = new postfx_class()
        fx_sprites = []

        for i in [0...num]
            sprite = new PIXI.Sprite(new PIXI.RenderTexture res, res)
            sprite.width = @render_width
            sprite.height = @render_height
            sprite.render = true

            this.addChild sprite
            fx_sprites.push sprite

        @postfx.push [fx, fx_sprites]
        return fx

    remove_postfx: (postfx_ref) ->
        for [fx, fx_sprites], index in @postfx
            if fx == postfx_ref
                fx.destroy()
                this.removeChild(sprite) for sprite in fx_sprites
                @postfx.splice index, 1
                return

        throw new Error "PostFx reference not found in stage."

    render_postfx: ->
        # Hide the postfx sprites before rendering so the fx won't recursively render itself.
        for [fx, fx_sprites] in @postfx
            for sprite in fx_sprites
                sprite.visible = false

        for [fx, fx_sprites] in @postfx
            res = fx.resolution
            # Size the viewport to the postfx resolution and send it off.
            fx.render fx_sprites, this, (this._set_viewport_to_resolution res, res)

        # Show the sprites for final rendering.
        for [fx, fx_sprites] in @postfx
            for sprite in fx_sprites when sprite.render
                sprite.visible = true

        # Reset viewport back to renderer resolution.
        this._set_viewport_to_resolution()

    mousemove_callback: (event) =>
        # Map mouse to canvas's physical DOM size (which may be smaller,
        # larger, or different aspect ratio than the render resolution/
        # canvas coordinate space depending on the CSS).
        event.map_position_to_DOM @mouse_position, @canvas_element

        # Now scale that to the actual internal coordinate space.
        @mouse_position[0] = @mouse_position[0] * (@render_width / @canvas_css_width)
        @mouse_position[1] = @mouse_position[1] * (@render_height / @canvas_css_height)

    update: (event) =>
        # Transform stage mouse coordinates to layer's.
        m = mat3.create()
        for _, layer of @layers._layers
            mat3.transpose m, layer.worldTransform
            mat3.invert m, m
            vec2.transformMat3 layer.mouse_position, @mouse_position, m


class PostFx extends PHObject
    resolution: 512
    buffers: 1

    render: (buffers, stage, viewport) ->
        null


class PerihelionWorld extends PHObject
    constructor: (@stage, @width, @height) ->
        Physics.init @width, @height
        new Actors.Player @width, @height

        @enemy_spawner = new EnemySpawner @width, @height
        @camera = new Camera @stage.viewport_width, @stage.viewport_height
        @hud = new Hud @stage.viewport_width, @stage.viewport_height

        @fx_screenshake = @stage.new_postfx ScreenShake
        # @fx_motionblur = @stage.new_postfx MotionBlur

        @bg_layer = @stage.layers.new_layer "bg"
        @main_layer = @stage.layers.new_layer "main"
        @generic_layer = @stage.layers.new_layer "generic"
        @overlay_layer = @stage.layers.new_layer "overlay"

        bg_sprite = new Background "bg", @width, @height
        bg_sprite.scale_viewport_world @stage, this, 0.25

        @bg_layer.set_parallax 4.0
        @bg_layer.addChild bg_sprite

        @overlay_layer.no_transforms()
        @overlay_layer.addChild @hud

        this.add_listener TickEvent, @update
        this.add_listener ActorDestroyEvent, @remove_actor_callback
        this.add_listener ActorCreateEvent, @add_actor_callback
        this.add_listener EntityDestroyEvent, @remove_entity_callback
        this.add_listener EntityCreateEvent, @add_entity_callback
        this.add_listener PlayerDamageEvent, @player_damage_callback

        Sound.play_loop "epicshitness"

    __destroy_world: ->
        Objects.destroy_all_entities()

        @enemy_spawner.destroy()
        @camera.destroy()
        @hud.destroy()

        @stage.reset()

    add_actor_callback: (event) =>
        @main_layer.addChild event.actor
        Physics.add_body event.actor.body

    remove_actor_callback: (event) =>
        @main_layer.removeChild event.actor
        Physics.remove_body event.actor.body

    add_entity_callback: (event) =>
        @generic_layer.addChild event.entity

    remove_entity_callback: (event) =>
        @generic_layer.removeChild event.entity

    player_damage_callback: (event, points) =>
        if event.player.alive
            @fx_screenshake.shake = 5
        else
            @fx_screenshake.shake = 20

    update: (event) =>
        delta_time = event.delta_time

        # Update camera.
        @camera.track_to_actor Objects.get_player_actor(), this, delta_time
        @stage.layers.pan @camera.x, @camera.y

    draw_debug: (context) ->
        if DEBUG
            # Pixi.js matrices are transponsed of glMatrix's.
            m = mat3.transpose mat3.create(), @main_layer.worldTransform
            Physics.draw_debug context, m

            for actor in @main_layer.children
                actor.draw_debug context, m


class ScreenShake extends PostFx
    resolution: 512
    buffers: 1
    shake_decay: 5

    constructor: ->
        super()
        @shake = 0
        @half_resolution = @resolution * 0.5

        this.add_listener TickEvent, @update

    render: (buffers, stage, viewport) ->
        for buffer in buffers
            if @shake
                buffer.render = true
                buffer.alpha = 0.8
                viewport.position.x = @half_resolution + Util.random_range(-@shake, @shake)
                viewport.position.y = @half_resolution + Util.random_range(-@shake, @shake)
                buffer.texture.render stage

            else
                buffer.render = false

    update: (event) =>
        @shake = Math.max 0, @shake - @shake_decay * event.delta_time


class MotionBlur extends PostFx
    resolution: 512
    buffers: 3

    constructor: ->
        super()
        @latest_buffer = 0
        @enable = true

    render: (buffers, stage, viewport) ->
        for i in [@latest_buffer...buffers.length]
            buffer = buffers[i % buffers.length]
            if @enable
                buffer.render = true
                buffer.alpha = 0.25
                if i == @latest_buffer
                    buffer.texture.render stage
            else
                buffer.render = false

        @latest_buffer = (@latest_buffer + 1) % buffers.length


class Hud extends mixOf PIXI.DisplayObjectContainer, PHObject
    constructor: (@viewport_width, @viewport_height) ->
        PIXI.DisplayObjectContainer.constructor.call(this)

        @cursor = new HudCursor()
        @health_bar = new HudHealthBar()
        @hit_flash = new HudHitFlash @viewport_width, @viewport_height

        this.addChild @cursor

        @health_bar.position.x = -(@viewport_width*0.5) + 2
        @health_bar.position.y = -(@viewport_height*0.5) + 2
        this.addChild @health_bar

        this.addChild @hit_flash

    __destroy_hud: ->
        @cursor.destroy()
        @health_bar.destroy()
        @hit_flash.destroy()


class HudCursor extends mixOf PIXI.DisplayObjectContainer, PHObject
    inner_texture_name: "cursor1"
    outer_texture_name: "cursor2"
    inner_size: 4
    outer_size: 10
    rotation_speed: 1.0

    constructor: ->
        PIXI.DisplayObjectContainer.constructor.call(this)
        this.add_listener TickEvent, @update

        @c1 = new PIXI.Sprite AssetManager.new_texture(@inner_texture_name)
        @c2 = new PIXI.Sprite AssetManager.new_texture(@outer_texture_name)

        @c1.width = @c1.height = @inner_size
        @c2.width = @c2.height = @outer_size
        @c1.anchor.x = @c1.anchor.y = @c2.anchor.x = @c2.anchor.y = 0.5

        @alpha = 0.7
        this.addChild @c1
        this.addChild @c2

    update: (event) =>
        delta_time = event.delta_time

        # @position.x = @parent.mouse_position[0]
        # @position.y = @parent.mouse_position[1]

        # Drill down parent hierachy until we find an ObjectLayer, PerihelionStage
        # or any other object with a mouse_position property.
        parent = @parent
        while parent
            if parent.mouse_position?
                @position.x = parent.mouse_position[0]
                @position.y = parent.mouse_position[1]
                break
            else
                parent = parent.parent

        @c1.rotation += @rotation_speed * delta_time
        @c2.rotation -= @rotation_speed * delta_time


class HudHealthBar extends mixOf PIXI.DisplayObjectContainer, PHObject
    pip_texture_name: "hud_healthpip"
    pip_spacing: 1.2
    pip_size: 2
    pip_sweeper_speed: 60
    pip_enlarger_speed: 40
    pip_enlarger_maxsize: 6

    constructor: (@max_health=null) ->
        PIXI.DisplayObjectContainer.constructor.call(this)
        this.add_listener PlayerDamageEvent, @player_damage_callback
        this.add_listener PlayerHealEvent, @player_heal_callback
        this.add_listener TickEvent, @update

        @pip_textures = AssetManager.new_texture @pip_texture_name
        @pip_gradations = @pip_textures.length - 1    # Don't count zero.
        @bar_length = 0
        @pips = []

        @max_health = @max_health or Objects.get_player_actor()?.max_hp or 6
        @max_health = Math.floor @max_health
        @current_health = @max_health

        # Pip-sweeper does exactly that -- goes the length of the
        # bar when the health ticks up. It takes the appearence of
        # a completely filled pip.
        @pip_sweeper = new PIXI.MovieClip(@pip_textures)
        @pip_sweeper.setTexture @pip_textures[@pip_textures.length-1]
        @pip_sweeper.visible = false

        # An enlarging effect whenever a pip is newly filled, similar
        # to pip sweeper
        @pip_enlarger = new PIXI.MovieClip(@pip_textures)
        @pip_enlarger.setTexture @pip_textures[@pip_textures.length-1]
        @pip_enlarger.visible = false

        @hp_healed = 0                  # Used for pip-sweeper.
        @hp_last_int = @max_health      # Used for pip-enlarger.

        this.reset_bar @max_health

    reset_bar: (max_health=null, current_health=null) ->
        @max_health = max_health or @max_health
        @current_health = current_health or @current_health

        @bar_length = @pip_spacing * (@max_health - 1)
        @pips.length = 0

        # Remove all children from DisplayObjectContainer.
        while @children.length > 0
            this.removeChild @children[0]

        # Create pips.
        for i in [0...@max_health]
            pip = new PIXI.MovieClip(@pip_textures)
            pip.position.x = i * @pip_spacing
            pip.width = pip.height = @pip_size
            pip.alpha = 0.65
            pip.anchor.x = pip.anchor.y = 0.5
            @pips.push pip
            this.addChild pip

        # Re-add sweeper and enlarger on top of them.
        this.addChild @pip_sweeper
        this.addChild @pip_enlarger

        # Re-set the appropriate health frames for the pips.
        this.set_health @current_health

    set_health: (points) ->
        # Each pip represents 1hp, with fractions of a point
        # representable by the number of frames available in
        # in the pip texture.
        for pip, index in @pips
            fraction = if points >= 1 then 1.0 else (points % 1)
            pip_tex_index = Math.floor(@pip_gradations * fraction)
            pip.setTexture @pip_textures[pip_tex_index]
            points = Math.max 0, points-1

    update: (event) =>
        delta_time = event.delta_time

        if @pip_sweeper.visible
            @pip_sweeper.position.x += @pip_sweeper_speed * delta_time
            if @pip_sweeper.position.x >= @bar_length
                @pip_sweeper.visible = false

        if @pip_enlarger.visible
            @pip_enlarger.width += @pip_enlarger_speed * delta_time
            @pip_enlarger.height += @pip_enlarger_speed * delta_time
            if @pip_enlarger.width >= @pip_enlarger_maxsize
                @pip_enlarger.visible = false

    start_pip_sweeper: ->
        # Set to position of first pip.
        @pip_sweeper.position.x = @pips[0].position.x
        @pip_sweeper.position.y = @pips[0].position.y
        @pip_sweeper.width = @pips[0].width
        @pip_sweeper.height = @pips[0].height
        @pip_sweeper.anchor.x = @pips[0].anchor.x
        @pip_sweeper.anchor.y = @pips[0].anchor.y
        @pip_sweeper.visible = true

    start_pip_enlarger: (pip_index) ->
        pip_index = Math.min pip_index, @pips.length-1
        @pip_enlarger.position.x = @pips[pip_index].position.x
        @pip_enlarger.position.y = @pips[pip_index].position.y
        @pip_enlarger.width = @pips[pip_index].width
        @pip_enlarger.height = @pips[pip_index].height
        @pip_enlarger.anchor.x = @pips[pip_index].anchor.x
        @pip_enlarger.anchor.y = @pips[pip_index].anchor.y
        @pip_enlarger.visible = true

    player_damage_callback: (event) =>
        @current_health = event.player.hp
        this.set_health @current_health

        @hp_healed = 0
        @hp_last_int = Math.floor event.player.hp

    player_heal_callback: (event) =>
        @hp_healed += event.points
        # Start pip sweeper whenever enough hp to advance a pip's frame is accumulated.
        # Second conditional is for when the bar is completely full -- rounding
        # errors force this special case.
        if @hp_healed >= (1.0 / @pip_gradations) or event.player.hp == event.player.max_hp
            this.start_pip_sweeper()
            @hp_healed = 0

        # When a pip has been filled.
        if Math.floor(event.player.hp) > @hp_last_int
            @hp_last_int = Math.floor event.player.hp
            this.start_pip_enlarger @hp_last_int-1
            Sound.emit "blip"

        @current_health = event.player.hp
        this.set_health @current_health


class HudHitFlash extends mixOf PIXI.Sprite, PHObject
    texture_name: "hit_flash"
    fade_speed: 4.0

    constructor: (width, height) ->
        PIXI.Sprite.constructor.call(this, AssetManager.new_texture(@texture_name))
        this.add_listener TickEvent, @update
        this.add_listener PlayerDamageEvent, @player_damage_callback

        @width = width
        @height = height
        @anchor.x = @anchor.y = 0.5
        @visible = false

    update: (event) =>
        if @alpha > 0.0
            @alpha = Math.max 0.0, @alpha - (@fade_speed * event.delta_time)
            if @alpha == 0 then @visible = false

    player_damage_callback: (event) =>
        @alpha = 1.0
        @visible = true


class Camera extends PHObject
    constructor: (@width, @height) ->
        this.add_listener TickEvent, @update

        @zoom = 1.0
        @rotation = 0.0
        @position = vec2.create()
        @x = 0
        @y = 0

        @velocity = vec2.create()
        @_steering = new Steering.Arrive()
        @_steering.max_acceleration = 100
        @_steering.max_speed = 150
        @_steering.slow_radius = 20
        @_steering.target_radius = 10
        @_steering.time_to_target = 1.5
        @_proxy_target = {
            position: vec2.create(),
            velocity: null
        }

        @motion_field = new Effects.MotionField 30, @width, @height

    track_to_actor: (actor, bounds, delta_time) ->
        if actor
            # Constrain the seek target so camera won't try to track beyond the bounds,
            # making the velocity at the edges out of whack and "sticking" the camera.
            [px, py] = @constrain bounds, actor.body.position[0], actor.body.position[1]

            # vec2.set @_proxy_target.position, px, py
            # @_proxy_target.velocity = actor.body.velocity

            # # Steering + simple Euler integration.
            # @_steering.steer this, @_proxy_target
            # vec2.add @velocity, @velocity, vec2.scale(@_steering.linear, @_steering.linear, delta_time)
            # vec2.scale @velocity, @velocity, 0.99   # Drag to keep the tracking stable.
            # vec2.add @position, @position, vec2.scale(vec2.create(), @velocity, delta_time)

            # Contrain the instantaneous camera position this time so that it won't
            # temporarily overshoot the bounds when tracking the target at high velocities.
            # [@position[0], @position[1]] = @constrain bounds, @position[0], @position[1]

            vec2.set @position, px, py
            @x = @position[0]
            @y = @position[1]

    constrain: (bounds, x, y) ->
        bounds_halfwidth = bounds.width * 0.5
        bounds_halfheight = bounds.height * 0.5
        halfwidth = @width * 0.5
        halfheight = @height * 0.5

        if @width < bounds.width
            x = Math.max -(bounds_halfwidth - halfwidth), x   # lower bound
            x = Math.min  (bounds_halfwidth - halfwidth), x   # upper bound
        else
            x = 0

        if @height < bounds.height
            y = Math.max -(bounds_halfheight - halfheight), y # lower bound
            y = Math.min  (bounds_halfheight - halfheight), y # upper bound
        else
            y = 0

        return [x,y]

    __destroy_camera: ->
        @motion_field.destroy()

    update: (event) =>
        @motion_field.set_new_position @position


class Background extends PIXI.Sprite
    constructor: (texture_name, bg_width, bg_height) ->
        PIXI.Sprite.constructor.call(this, AssetManager.new_texture(texture_name))

        # Chop off top/bottom or left/right of the texture to match
        # the aspect ratio of the specified background dimensions.
        tex_aspect_ratio = @texture.width / @texture.height
        bg_aspect_ratio = bg_width / bg_height

        # Texture is wider; chop width to match.
        if tex_aspect_ratio >= bg_aspect_ratio
            @texture.frame.width = @texture.frame.height * bg_aspect_ratio
        # Texture is taller; chop height to match.
        else
            @texture.frame.height =  @texture.width * (1.0 / bg_aspect_ratio)

        # Center texture frame.
        @texture.frame.x = (@texture.width * 0.5) - (@texture.frame.width * 0.5)
        @texture.frame.y = (@texture.height * 0.5) - (@texture.frame.height * 0.5)

        @aspect_ratio = bg_aspect_ratio
        @anchor.x = @anchor.y = 0.5

    fit: (target_width, target_height) ->
        # Fit to specified dimensions while keeping the
        # initial aspect ratio of the texture.
        target_aspect_ratio = target_width / target_height

        # Target is wider (or aspects are the same) -- stretch
        # width to meet it.
        if target_aspect_ratio >= @aspect_ratio
            @width = target_width
            @height =  @width * (1.0 / @aspect_ratio)
        # Target is higher -- stretch height.
        else
            @height = target_height
            @width = @height * @aspect_ratio

    scale_viewport_world: (stage, world, factor) ->
        # Scale between the stage's rendering viewport and the world size,
        # where factor=0 is the viewport size and factor=1 is the world size.
        # Use this with ObjectLayer parallax for neat scrolling effects.
        w = stage.viewport_width + ((world.width - stage.viewport_width) * factor)
        h = stage.viewport_height + ((world.height - stage.viewport_height) * factor)
        this.fit w, h


class ObjectLayer extends PIXI.DisplayObjectContainer
    no_transform: true

    constructor: () ->
        PIXI.DisplayObjectContainer.constructor.call(this)

        # .pivot acts as an origin offset, so we
        # pan using it instead of .position in order
        # to keep the center of rotation at
        # (position.x, position.y)
        @pan_tween = new createjs.Tween @pivot
        @zoom_tween = new createjs.Tween @scale
        @rotate_tween = new createjs.Tween this

        @rotation_multiplier = 1.0
        @zoom_multiplier = 1.0
        @pan_multiplier = 1.0

        @mouse_position = vec2.create()

    pan: (x, y, ms, easing) ->
        x = 0 + (x * @pan_multiplier)
        y = 0 + (y * @pan_multiplier)

        if ms
            @pan_tween.to {x: x, y: y}, ms, easing
        else
            @pivot.x = x
            @pivot.y = y

    rotate: (rads, ms, easing) ->
        rads = 0 + (rads * @rotation_multiplier)

        if ms
            @rotate_tween.to {rotation: rads}, ms, easing
        else
            @rotation = rads

    zoom: (factor, ms, easing) ->
        # Zoom is based on 1.0, not 0.0 like panning and rotation
        factor = 1.0 + ((factor - 1.0) * @zoom_multiplier)

        if ms
            @zoom_tween.to {x: factor, y: factor}, ms, easing
        else
            @scale.x = @scale.y = factor

    set_parallax: (factor) ->
        @zoom_multiplier = @pan_multiplier = (1.0 / factor)

    no_transforms: ->
        @zoom_multiplier = @pan_multiplier = @rotation_multiplier = 0.0


class ObjectLayerCollection
    constructor: (@stage) ->
        @_layers = {}

    new_layer: (layer_name) ->
        if layer_name not of @_layers
            layer = new ObjectLayer()
            @stage.viewport.addChild layer
            @_layers[layer_name] = layer
            return layer
        else
            throw new Error "Layer name already exists."

    get_layer: (layer_name) ->
        return @_layers[layer_name]

    pan: (x, y, ms, easing) ->
        for _, layer of @_layers
            layer.pan x, y, ms, easing

    rotate: (degrees, ms, easing) ->
        for _, layer of @_layers
            layer.rotate degrees, ms, easing

    zoom: (factor, ms, easing) ->
        for _, layer of @_layers
            layer.zoom factor, ms, easing
