mixOf = (base, mixins...) ->
    class MixedClass extends base
        null

    for mixin in mixins by -1 # Mixins defined first overrides latter ones.
        for name, property of mixin.prototype
            MixedClass.prototype[name] = property

    return MixedClass


class Destructable
    destroy: (args...) ->
        if not @__destroyed?
            # Call all methods prepended with __destroy.
            for name, property of this when name.substring(0, 9) == "__destroy"
                property.apply(this, args)
            @__destroyed = true
