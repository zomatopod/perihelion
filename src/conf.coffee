# This is inserted conditionally in the Cakefile script depending on the build target,
# so it does not have to be changed explictly here.
# The peculiar construction is so that UglifyJS's global_def stripping will work correctly.
# http://jstarrdewar.com/blog/2013/02/28/use-uglify-to-automatically-strip-debug-messages-from-your-javascript
# `if (typeof DEBUG === 'undefined') DEBUG = true;`     // Change boolean as necessary.

ASSET_MANIFEST = "asset_manifest"

# This is the internal rendering resolution, which is
# independant of the canvas's size. The buffer will be
# scaled to fit the canvas.
RENDER_WIDTH = 1600
RENDER_HEIGHT = 900

QUADTREE_MAX_DEPTH = 5
QUADTREE_MAX_LEAF_ELEMENTS = 3

VERLET_CONSTRAINT_INTERATIONS = 4
PHYSICS_TICKRATE = 60
PHYSICS_MAX_STEPS = 5

DEBUG_DRAW_BOUNDS = false
DEBUG_DRAW_BODIES = false
DEBUG_DRAW_QUADTREE = false
