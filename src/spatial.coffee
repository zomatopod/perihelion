#_require conf.coffee
#_require util.coffee
#_require collision.coffee


class AABB
    constructor: (@daughter_shape) ->
        @width = 0
        @height = 0
        @tl = vec2.create()
        @br = vec2.create()
        @center = null

        @update_transform null

    set: (dim) ->
        @daughter_shape.set dim
        @update_transform null

    intersects_aabb: (test_aabb) ->
        if test_aabb not instanceof AABB
            throw new Error "AABB received a non-instance of AABB to test."
        return Intersection.intersects_aabb @center, @width, @height, test_aabb.center, test_aabb.width, test_aabb.height

    encloses_aabb: (test_aabb) ->
        if test_aabb not instanceof AABB
            throw new Error "AABB received a non-instance of AABB to test."
        return Intersection.encloses_aabb @center, @width, @height, test_aabb.center, test_aabb.width, test_aabb.height

    intersects: (test_aabb) ->
        if @intersects_aabb test_aabb
            return  @daughter_shape.intersects test_aabb.daughter_shape
        else
            return false

    update_transform: ->
        @daughter_shape.update_transform

        # Find minimum bounding rectangle for the transformed vertices.
        @tl[0] = @tl[1] = Number.MAX_VALUE
        @br[0] = @br[1] = -Number.MAX_VALUE

        for vertex in @daughter_shape.world_vertices
            if vertex[0] < @tl[0] then @tl[0] = vertex[0]
            if vertex[0] > @br[0] then @br[0] = vertex[0]

            if vertex[1] < @tl[1] then @tl[1] = vertex[1]
            if vertex[1] > @br[1] then @br[1] = vertex[1]

        @width = @br[0] - @tl[0]
        @height = @br[1] - @tl[1]
        @center = @daughter_shape.world_center

    draw_debug: (context, transform) ->
        p = vec2.clone @tl
        q = vec2.clone @br

        vec2.transformMat3 p, p, transform
        vec2.transformMat3 q, q, transform

        context.strokeStyle = "blue"
        context.strokeRect p[0], p[1], q[0] - p[0], q[1] - p[1]


class OBB
    constructor: (dim) ->
        @local_vertices = Array 4
        @world_vertices = Array 4

        for i in [0...4]
            @local_vertices[i] = vec2.create()
            @world_vertices[i] = vec2.create()

        @local_center = vec2.create()
        @world_center = vec2.create()

        @set dim

    set: (dim) ->
        dim.cx = dim.cx or 0
        dim.cy = dim.cy or 0
        dim.height = dim.height or 1
        dim.width = dim.width or 1
        dim.halfwidth = dim.width * 0.5
        dim.halfheight = dim.height * 0.5

        # Set the four vertices of the rectangle in CCW order.
        vec2.set @local_vertices[0], dim.cx - dim.halfwidth, dim.cy - dim.halfheight
        vec2.set @local_vertices[1], dim.cx - dim.halfwidth, dim.cy + dim.halfheight
        vec2.set @local_vertices[2], dim.cx + dim.halfwidth, dim.cy + dim.halfheight
        vec2.set @local_vertices[3], dim.cx + dim.halfwidth, dim.cy - dim.halfheight

        # Copy local vertices to global.
        vec2.set @world_vertices[0], @local_vertices[0][0], @local_vertices[0][1]
        vec2.set @world_vertices[1], @local_vertices[1][0], @local_vertices[1][1]
        vec2.set @world_vertices[2], @local_vertices[2][0], @local_vertices[2][1]
        vec2.set @world_vertices[3], @local_vertices[3][0], @local_vertices[3][1]

        vec2.set @local_center, dim.cx, dim.cy
        vec2.set @world_center, @local_center[0], @local_center[1]

    intersects: (test_obb) ->
        if test_obb not instanceof OBB
            throw new Error "OBB received a non-instance of OBB to test."
        return Intersection.intersects_convex @world_vertices, test_obb.world_vertices

    update_transform: ->
        # TODO: Invalidated after switching to Pixi.js
        # if entity?
        #     # Recalculate global-space vertices with the target object's transform.
        #     obj_mtx = entity.get_transform()

        #     # Bounding box.
        #     for vertex, index in @local_vertices
        #         mtx = obj_mtx.clone()                               # (EaselJS's matrix API is pretty ghetto.)
        #         mtx.append 1, 0, 0, 1, vertex[0], vertex[1]         # Multiply vertex by transform matrix.
        #         vec2.set @world_vertices[index], mtx.tx, mtx.ty

        #     # Bounds center.
        #     mtx = obj_mtx.clone()
        #     mtx.append 1, 0, 0, 1, @local_center[0], @local_center[1]
        #     vec2.set @world_center, mtx.tx, mtx.ty
        #     @cx = @world_center[0]
        #     @cy = @world_center[1]

    draw_debug: (shape) ->
        # shape.graphics.beginStroke("red")
        # shape.graphics.lineTo @world_vertices[0][0], @world_vertices[0][1]
        # shape.graphics.lineTo @world_vertices[1][0], @world_vertices[1][1]
        # shape.graphics.lineTo @world_vertices[2][0], @world_vertices[2][1]
        # shape.graphics.lineTo @world_vertices[3][0], @world_vertices[3][1]
        # shape.graphics.lineTo @world_vertices[0][0], @world_vertices[0][1]
        # shape.graphics.endStroke()


class BasicRect
    constructor: (dim) ->
        @world_center = vec2.create()
        @world_vertices = Array 4
        for i in [0...4]
            @world_vertices[i] = vec2.create()

        @set dim

    set: (dim) ->
        dim.cx = dim.cx or 0
        dim.cy = dim.cy or 0
        dim.height = dim.height or 1
        dim.width = dim.width or 1
        dim.halfwidth = dim.width * 0.5
        dim.halfheight = dim.height * 0.5

        vec2.set @world_vertices[0], dim.cx - dim.halfwidth, dim.cy - dim.halfheight
        vec2.set @world_vertices[1], dim.cx - dim.halfwidth, dim.cy + dim.halfheight
        vec2.set @world_vertices[2], dim.cx + dim.halfwidth, dim.cy + dim.halfheight
        vec2.set @world_vertices[3], dim.cx + dim.halfwidth, dim.cy - dim.halfheight

        vec2.set @world_center, dim.cx, dim.cy

    intersects: (test_rect) ->
        # As this class is meant to be a data filler for AABB, this will always
        # return true because the AABB parent would have already performed an
        # AABB-AABB test.
        return true

    update_transform: ->
        return

    draw_debug: (shape) ->
        return


class BaseAABBDaughterShape
    constructor: ->
        @world_center = vec2.create()
        @world_vertices = []

    set: (dim) ->

    intersects: (test_rect) ->
        return true

    update_transform: ->
        return

    draw_debug: (shape) ->
        return


class Quadtree
    constructor: (@root_dim) ->
        max_nodes = 0
        for i in [0...QUADTREE_MAX_DEPTH]
            max_nodes += Math.pow 4, i

        @_node_buffer = new Util.RingBuffer max_nodes, "node_reset", =>
            return new QuadtreeNode {}, this

        @_root = @get_new_node @root_dim, 0

    get_new_node: (dim, level=0) ->
        node = @_node_buffer.get dim, level
        return node

    insert: (object, object_aabb=null) ->
        @_root.insert object, object_aabb

    clear: ->
        # Clear old nodes so we don't hold references longer than we have to
        # before the buffer comes around again.
        @_root.clear_elements()
        @_root = @get_new_node @root_dim, 0

    broad_query: (query_aabb, candidates=[]) ->
        return @_root.broad_query query_aabb

    draw_debug: (context, transform) ->
        @_root.draw_debug context, transform


class QuadtreeNode
    constructor: (dim, @quadtree_parent, @level=0) ->
        @elements = []             # elements = [[object1, object_bounds1], [object1, object_bounds2], ...]
        @leaves = Array 4
        @bounds = new AABB(new BasicRect dim)

        @has_children = false

    node_reset: (dim, level) ->
        @level = level
        @has_children = false
        @bounds.set dim

    clear_elements: ->
        @elements.length = 0
        if @is_internal()
            for leaf in @leaves
                leaf.clear_elements()

    is_internal: ->
        return @has_children

    is_leaf: ->
        return not @has_children

    find_leaf_quadrant: (test_aabb) ->
        # If the box is not completely within the internal node's
        # bounds (or the node is a leaf without any subdivisions),
        # then null is returned.
        quadrant = null

        # Only internal nodes have subdivided quadrants.
        if @is_internal()
            for i in [0...4]
                if @leaves[i].bounds.encloses_aabb test_aabb
                    quadrant = i
                    break

        return quadrant

    subdivide: ->
        # Only leaf nodes can be subdivided internal nodes
        # have already been divided once.
        if @is_leaf()
            height = @bounds.height * 0.5
            width = @bounds.width * 0.5
            halfheight = height * 0.5
            halfwidth = width * 0.5
            cx = @bounds.center[0]
            cy = @bounds.center[1]
            level = @level + 1

            # New children in order from quadrants 1-4
            @leaves[0] = @quadtree_parent.get_new_node {cx: cx + halfwidth, cy: cy + halfheight, width: width, height: height}, level
            @leaves[1] = @quadtree_parent.get_new_node {cx: cx - halfwidth, cy: cy + halfheight, width: width, height: height}, level
            @leaves[2] = @quadtree_parent.get_new_node {cx: cx - halfwidth, cy: cy - halfheight, width: width, height: height}, level
            @leaves[3] = @quadtree_parent.get_new_node {cx: cx + halfwidth, cy: cy - halfheight, width: width, height: height}, level

            @has_children = true

    insert: (object, object_aabb=null) ->
        object_aabb = object_aabb or object.aabb
        if not object_aabb then throw new Error "Could not find an AABB to insert object into quadtree."

        # Find an appropriate child node to insert into.
        quadrant = @find_leaf_quadrant object_aabb
        if quadrant?
            @leaves[quadrant].insert object

        # Doesn't fit into a child (or this node itself is a leaf and there are no children to begin with),
        # so insert into self.
        else
            @elements.push [object, object_aabb]

            # Split if full. Only leaf nodes can be split.
            if @is_leaf() and @elements.length > QUADTREE_MAX_LEAF_ELEMENTS and (@level + 1) < QUADTREE_MAX_DEPTH
                @subdivide()

                # Try to move as many elements as possible down to the new leaf nodes.
                index = 0
                while index < @elements.length
                    [object, object_aabb] = @elements[index]
                    quadrant = @find_leaf_quadrant object_aabb

                    if quadrant?
                        @leaves[quadrant].insert object, object_aabb
                        @elements.splice index, 1
                    else
                        index += 1

    # Returns all elements that MAY intersect with a query box.
    broad_query: (query_aabb, candidates=[]) ->
        if query_aabb.intersects_aabb @bounds
            candidates = candidates.concat @elements

            if @is_internal()
                for child, index in @leaves
                    candidates = child.broad_query query_aabb, candidates

        return candidates

    draw_debug: (context, transform) ->
        p = vec2.clone @bounds.tl
        q = vec2.clone @bounds.br
        c = vec2.clone @bounds.center

        vec2.transformMat3 p, p, transform
        vec2.transformMat3 q, q, transform
        vec2.transformMat3 c, c, transform

        context.strokeStyle = "green"
        context.fillStyle = "green"
        context.strokeRect p[0], p[1], q[0] - p[0], q[1] - p[1]

        context.beginPath()
        context.arc c[0], c[1], 4, 0, Math.PI * 2
        context.fill()

        if @is_internal()
            for child in @leaves
                child.draw_debug context, transform

    toString: ->
        indent = Array(@level).join '  '
        ret = ""

        if @is_internal()
            ret += indent + "LEAF:" + @elements.length + "\n"
        else
            ret += indent + "INTERNAL:" + @elements.length + "\n"
            for child, index in @leaves
                ret += child.toString()

        return ret
