Util = {}


class Util.RingBuffer
    constructor: (@size, @init_property=null, e_create_callback=null) ->
        @_buffer = Array @size
        @ptr = 0

        if e_create_callback?
            for i in [0...@size]
                @_buffer[i] = e_create_callback()

    get: (init_args...) ->
        e = @_buffer[@ptr]
        @ptr = (@ptr + 1) % @size

        if @init_property?
            e[@init_property] init_args...

        return e

    set: (i, v) ->
        if 0 <= i < @size
            @_buffer[i] = v
        else
            throw new RangeError "Index #{i} is out of bounds of [0, #{@size - 1}]"


class Util.Pool
    constructor: (@size, @init_property=null, e_create_callback=null) ->
        @_buffer = Array @size
        @ptr = 0

        if e_create_callback?
            for i in [0...@size]
                @_buffer[i] = e_create_callback()

    get: (init_args...) ->
        # Find a free element.
        for _ in [0...@size]
            e = @_buffer[@ptr]
            @ptr = (@ptr + 1) % @size

            # Is free or doesn't have the .free property, whcih
            # means this is the first time this element has been
            # requested.
            if not e.free? or e.free == true
                e.free = false

                if @init_property?
                    e[@init_property] init_args...

                return e

        throw new Error "Pool has been exhausted of all #{@size} elements"

    free_all: ->
        for e in @_buffer
            e.free = true


Util.create_vector_pool = (size) ->
    _vectors = new Util.Pool size, null, ->
        return vec2.create()

    _stack = [[]]
    _stack_idx = 0

    get_vector = (x=0.0, y=0.0) ->
        new_vec = _vectors.get()
        vec2.set new_vec, x, y

        _stack[_stack_idx].push new_vec
        return new_vec

    get_vector.push = ->
        _stack.push []
        _stack_idx += 1

    get_vector.pop = ->
        if _stack_idx > 0
            get_vector.free()
            _stack.pop()
            _stack_idx -= 1
        else
            throw new Error "Nothing to pop."

    get_vector.free = ->
        for v in _stack[_stack_idx]
            v.free = true

    get_vector.free_all = ->
        _vectors.free_all()
        _stack = [[]]

    return get_vector


class Util.Accumulator
    constructor: (@maximum, @max_drain_count=Number.MAX_VALUE, @overflow_callback=null) ->
        @size = 0
        @overflow_amount = 0

    add: (v) ->
        @size += v
        @overflow_amount = 0

        if @size > @maximum
            return @_drain()
        else
            return 0

    clear: ->
        @size = @overflow_amount = 0

    _drain: ->
        # Floating-point division error causes this to be less
        # than accurate to the amount we need to drain somtimes,
        # so we'll use a subtractive loop instead.
        # @overflow_amount = amount = Math.floor(@size / @maximum)
        # @size -= @maximum * amount

        @overflow_amount = amount = 0

        while @size > @maximum
            @overflow_amount += 1
            @size -= @maximum

            if amount < @max_drain_count
                amount += 1
                @overflow_callback() if @overflow_callback?

        return amount


Util.map_to_range = (value) ->
    r = value

    if value > Math.PI
        r -= 2*Math.PI
        r = Util.map_to_range(r)
    else if value < -Math.PI
        r += 2*Math.PI
        r = Util.map_to_range(r)

    return r


Util.vec2_eq = (v1, v2) ->
    return (v1[0] == v2[0] and v1[1] == v2[1])


Util.get_css = (dom_element, property) ->
    css = null
    if window.getComputedStyle
        css = document.defaultView.getComputedStyle(dom_element, null).getPropertyValue(property)
    return css


do ->
    _request_animation_frame = window.requestAnimationFrame or window.mozRequestAnimationFrame or
                               window.webkitRequestAnimationFrame or window.msRequestAnimationFrame

    # Must wrap in a function or you'll get an invocation error.
    Util.request_animation_frame = (callback) ->
        _request_animation_frame callback


do ->
    _perf_now = null
    _perf_now_names = ['now', 'webkitNow', 'msNow', 'mozNow']

    if "performance" of window
        for name in _perf_now_names
            if name of window.performance
                _perf_now = ->
                    return window.performance[name]()
                break

    if not _perf_now
        _perf_now = Date.now

    Util.hires_time = _perf_now


class Util.Timer
    constructor: ->
        # Time and last time are in (fractional with performance
        # timer) milliseconds.
        @_last_time = 0
        @_time = 0

        # Delta and elapsed are in fractional seconds.
        @delta = 0
        @elapsed = 0

    update: ->
        @_time = Util.hires_time()
        @delta = (@_time - @_last_time) * 0.001
        @elapsed += @delta
        @_last_time = @_time

    reset: ->
        @_last_time = @_time = @delta = @elapsed = 0


Util.vector_from_angle = (out, angle, magnitude=1.0) ->
    vec2.set out, Math.cos(angle), Math.sin(angle)
    return vec2.scale out, out, magnitude


Util.angle_from_vector = (v) ->
    return Math.atan2 v[1], v[0]


Util.random_range = (min, max) ->
    return Math.random() * (max - min) + min


Util.random_range_int = (min, max) ->
    return Math.floor(Util.random_range(min, max))


Util.random_choice = (array) ->
    return array[Util.random_range_int(0, array.length)]


Util.random_half = (v) ->
    return Util.random_range -(v*0.5), (v*0.5)