#_require base
#_require event
#_require objects
#_require steering
#_require effects


Actors.Enemies = {}

Actors.enemy_projectiles = {
    "standard" : {
        texture_name: "enemy_bullet"
        sprite_size: [1.5, 1.5]
        body_size: [0.5, 0.5]
        damage: 1
        lifetime: 4
        sound_create: "enemy_shoot"
    }

    "missile" : {
        texture_name: "missile1"
        sprite_size: [1.5, 1.5]
        body_size: [1.0, 1.0]
        impulse: 400
        speed: 30
        damage: 2
        lifetime: 10
        sound_create: "launch"
    }
}


class EnemySpawner extends mixOf Destructable, EventListenable
    constructor: (@world_width, @world_height) ->
        this.add_listener ActorDestroyEvent, this.actor_destroy_callback
        this.add_listener TickEvent, this.update

        @threat_level = 0
        @threat_level_cap = 2
        @threat_attrition = 0.2     # Rate at which cap automatically increases.

        @world_halfwidth = @world_width * 0.5
        @world_halfheight = @world_height * 0.5

        @enemy_classes = []

        # Collect all classes of Actors.Enemies.
        for name, cls of Actors.Enemies
            if cls != Actors.Enemies.Base
                @enemy_classes.push cls

    actor_destroy_callback: (event) =>
        if event.actor instanceof Actors.Enemies.Base
            @threat_level -= event.actor.threat_value

            # Threat cap does not increase when enemy is removed from
            # play without being destroyed (e.g. out of bounds).
            if not event.is_removed
                @threat_level_cap += event.actor.threat_increase

    update: (event) =>
        delta_time = event.delta_time

        if Objects.get_player_actor()
            @threat_level_cap += delta_time * @threat_attrition

            # Spawn enemies up to the cap (overflow is allowed, but no
            # enemies will spawn until the level recedes).
            if @threat_level < @threat_level_cap
                # Select a random enemy meeting the minimum threat cap.
                while true
                    enemy_class = Util.random_choice @enemy_classes
                    if enemy_class::min_threat_cap <= @threat_level_cap then break

                # Random position at the top, bottom, left, or right of the playfield.
                p = switch Util.random_range_int(0, 4)
                    when 0
                        vec2.fromValues Util.random_range(-@world_halfwidth, @world_halfwidth), -@world_halfheight
                    when 1
                        vec2.fromValues Util.random_range(-@world_halfwidth, @world_halfwidth), @world_halfheight
                    when 2
                        vec2.fromValues -@world_halfwidth, Util.random_range(-@world_halfheight, @world_halfheight)
                    when 3
                        vec2.fromValues @world_halfwidth, Util.random_range(-@world_halfheight, @world_halfheight)

                # Initial direction at player.
                d = vec2.sub vec2.create(), Objects.get_player_actor().body.position, p
                enemy = new enemy_class(p, @world_width, @world_height)
                enemy.body.set_rotation Util.angle_from_vector(d)

                @threat_level += enemy.threat_value


class Actors.Enemies.Base extends Objects.Actor
    threat_value: 1             # How much of the threat cap this enemy takes.
    threat_increase: 1          # When killed, how much does it increase the cap.
    min_threat_cap: 0           # The minimum threat cap before it begins spawning
    impulse: 20
    speed: 20
    range: 100
    fire_cone: Math.PI * 2.0
    weapon_system: {}

    sound_damage: "enemy_hit"

    constructor: (initial_position=null, @bounds_width=0, @bounds_height=0) ->
        super null, initial_position
        @weapon = new WeaponSystem this, @weapon_system
        @range_sqr = @range*@range

        @body.max_speed = @speed

    update: (event) ->
        delta_time = event.delta_time
        target = Objects.get_player_actor()

        d = Number.MAX_VALUE

        if target
            # Determine if target is in firing range.
            v = vec2.sub(vec2.create(), target.body.position, @body.position)
            d = vec2.sqrLen v
            t = Util.angle_from_vector(v)

        if d <= @range_sqr
            this._on_in_range target, t, Math.sqrt(d)
        else
            this._on_out_range()

        # Weapon system needs to cycle every update.
        @weapon.fire_cycle delta_time, t, target

        this.move delta_time
        this.check_world_bounds()

    move: (delta_time) ->
        null

    check_world_bounds: ->
        if @body.position[0] >  (@bounds_width * 0.5) + @width or
           @body.position[0] < -(@bounds_width * 0.5) - @width or
           @body.position[1] >  (@bounds_height * 0.5) + @height or
           @body.position[1] < -(@bounds_height * 0.5) - @height
            this.remove()

    _on_in_range: (target_actor, angle, distance) ->
        if Math.abs(Util.map_to_range(@body.rotation - angle)) <= (@fire_cone * 0.5)
            @weapon.firing = true
        else
            @weapon.firing = false

    _on_out_range: ->
        @weapon.firing = false

    __destroy_enemy_base: (args...) ->
        # Explosion effects when destroyed and not just removed from play.
        if not args[0]
            for _ in [0...Util.random_range_int(1,5)]
                new Effects.Debris @body.position

            Effects.explosion @body.position


class Actors.Enemies.Trash extends Actors.Enemies.Base
    texture_name: "enemy1"
    sprite_size: [2, 2]
    body_size: [2, 2]

    hp: 3
    threat_value: 1
    threat_increase: 1
    min_threat_cap: 0
    impulse: 20
    speed: 5
    range: 30

    weapon_system: {
        rate: 0.5
        muzzle_speed: 20
        projectile_class: Objects.BaseProjectile
        projectile_attrs: Actors.enemy_projectiles["standard"]
    }

    constructor: (initial_position, @bounds_width, @bounds_height) ->
        super initial_position, @bounds_width, @bounds_height
        @thrust_vec = vec2.create()

    move: (delta_time) ->
        # Thrust forward at current heading.
        Util.vector_from_angle @thrust_vec, @body.rotation, @impulse
        @body.add_force @thrust_vec


class Actors.Enemies.BetterTrash extends Actors.Enemies.Trash
    threat_value: 2
    threat_increase: 2
    min_threat_cap: 15
    speed: 7
    range: 40

    weapon_system: {
        rate: 1.0
        muzzle_speed: 25
        projectile_class: Objects.BaseProjectile
        projectile_attrs: Actors.enemy_projectiles["standard"]
    }


class Actors.Enemies.SuperTrash extends Actors.Enemies.Trash
    sprite_size: [3, 3]
    body_size: [2, 2]

    hp: 6
    threat_value: 5
    threat_increase: 3
    min_threat_cap: 25
    speed: 5
    range: 40

    weapon_system: {
        rate: 20
        muzzle_speed: 25
        salvo: 5
        salvo_delay: 3
        projectile_class: Objects.BaseProjectile
        projectile_attrs: Actors.enemy_projectiles["standard"]
    }


class Actors.Enemies.TrashMk2 extends Actors.Enemies.Trash
    threat_increase: 5
    min_threat_cap: 60
    speed: 7
    range: 40

    weapon_system: {
        rate: 1.0
        muzzle_speed: 30
        projectile_class: Objects.BaseProjectile
        projectile_attrs: Actors.enemy_projectiles["standard"]
        fire: (weapon_system, fire_info) ->
            fire_info.angle += 0.2
            weapon_system.fire_projectile fire_info
            fire_info.angle -= 0.4
            weapon_system.fire_projectile fire_info
    }


class Actors.Enemies.SuperTrashMk2 extends Actors.Enemies.SuperTrash
    threat_increase: 5
    min_threat_cap: 60
    speed: 15
    range: 50

    weapon_system: {
        rate: 20
        muzzle_speed: 25
        salvo: 5
        salvo_delay: 3
        projectile_class: Objects.BaseProjectile
        projectile_attrs: Actors.enemy_projectiles["standard"]
        fire: Actors.Enemies.TrashMk2::weapon_system.fire
    }


class Actors.Enemies.Turret extends Actors.Enemies.Base
    texture_name: "enemy3"
    sprite_size: [4, 4]
    body_size: [3, 3]

    hp: 6
    threat_value: 15
    threat_increase: 8
    min_threat_cap: 40
    impulse: 20
    speed: 5
    range: 40
    fire_cone: 0.1

    turn_speed: 1

    weapon_system: {
        rate: 4
        muzzle_speed: 24
        salvo: 4
        salvo_delay: 5
        projectile_class: Objects.BaseProjectile
        projectile_attrs: Actors.enemy_projectiles["standard"]
        fire: (weapon_system, fire_info) ->
            # Turret can only shoot straight ahead, hence using the body.rotation
            # instead of the derived angle.
            fire_info.angle = fire_info.owner_actor.body.rotation
            weapon_system.fire_projectile fire_info
            fire_info.angle = fire_info.owner_actor.body.rotation-0.1
            weapon_system.fire_projectile fire_info
            fire_info.angle = fire_info.owner_actor.body.rotation+0.1
            weapon_system.fire_projectile fire_info
    }

    constructor: (initial_position, @bounds_width, @bounds_height) ->
        super initial_position, @bounds_width, @bounds_height
        @face = new Steering.Face()
        @arrive = new Steering.Arrive()

        @arrive.max_acceleration = @impulse
        @face.max_rotational_speed = @turn_speed
        @face.max_angular_accel = 10

        @standby_point = { position: vec2.create() }
        @time_since_last_shot = 0

    select_standby_point: ->
        @standby_point.position[0] = Util.random_half @bounds_width
        @standby_point.position[1] = Util.random_half @bounds_height

    move: (delta_time) ->
        target = Objects.get_player_actor()

        if not @weapon.firing
            @time_since_last_shot += delta_time
            if @time_since_last_shot > 10
                this.select_standby_point()
                @time_since_last_shot = 0
        else
            @time_since_last_shot = 0

        if target
            # Face player.
            @face.steer this.body, target.body
            @body.set_rotation @body.rotation + (@face.angular * delta_time)

        # Go to standby point.
        @arrive.steer this.body, @standby_point
        @body.add_force @arrive.linear

        @body.clear_angular_velocity()


class Actors.Enemies.TurretMk2 extends Actors.Enemies.Turret
    hp: 6
    threat_value: 15
    threat_increase: 10
    min_threat_cap: 80
    speed: 15
    range: 90

    turn_speed: 2

    weapon_system: {
        rate: 4
        muzzle_speed: 30
        salvo: 4
        salvo_delay: 5
        projectile_class: Objects.BaseProjectile
        projectile_attrs: Actors.enemy_projectiles["standard"]
        fire: Actors.Enemies.Turret::weapon_system.fire
    }


class Actors.Enemies.MissileTurret extends Actors.Enemies.Turret
    texture_name: "enemy4"
    sprite_size: [5, 5]
    body_size: [3, 3]

    hp: 10
    threat_value: 20
    threat_increase: 8
    min_threat_cap: 60
    speed: 5
    range: 80

    weapon_system: {
        rate: 0.1
        muzzle_speed: 24
        projectile_class: Objects.BaseMissile
        projectile_attrs: Actors.enemy_projectiles["missile"]
    }
