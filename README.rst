==========
Perihelion
==========

Perihelion is an unsophisticated 2D, top-down shooter that runs in a modern HTML5 browser.


Building
========

Perihelion requires CoffeeScript_ and Rehab_ to build, and thus by extension, Node.js_. Assuming you have the appropriate flavor of Node already installed on your platform, ``npm`` the required packages::

    $ sudo npm install -g coffee-script rehab

You must ensure that your global npm modules directory is reachable in your path environment or symlink both modules to the project directory using ``npm link``. Alternatively, you may install the modules locally to the project directory by omitting the ``-g`` flag.

Then, execute the build task::

    $ cake build

(If you installed CoffeeScript locally, ``cake`` will exist under ``node_modules/.bin``.)

The build script can optionally lint the sources before compilation (useful for code hacking as it occurs before the .coffee sources are concatenated together and fed into the compiler) and minify the resultant JavaScript using CoffeeLint_ and UglifyJS2_, respectively. Run ``cake`` by itself for a list of options.


Library Credits
===============

* PIXI.js_: A neat little JavaScript WebGL and canvas renderer.
* CreateJS_: Sundry items such as preloading.


Bogus History
=============

Way back in high school, clearly a kid with too much time and not one inkling of sense to utilize it properly, I began a programming project at the start of a one-week winter break called Helion. The compiler spent this week of agonizing labor to birth Helion from several thousand lines of dubious C/C++ code. It was a silly thing, a questionable attempt at the recreation of a frenetic, Robotron 2048-style shooter made by someone with much less programming and game design sense than people in the early 80s. But it was good fun working on it. I spend the next few weeks adding other stuff that made it less or more broken.

I rediscovered the source code to Helion, and it still runs to my bemusement, excepting some visual artifacts owing from the bizarre way it implemented blend modes and scoreboards and death screens that didn't show up because it only flipped the backbuffer once after rendering them, which was rickety back then and completely busted now. So I thought, hey, should be a neat weekend project to rewrite this and make it whole again. Challenge Mode: Do it in a browser. It's the Information Superhighway Age, son!

Somewhere along the way, it all went terribly wrong and I found myself with the horrific realization that I was writing the world's cheapest physics engine in JavaScript to "rewrite" a game originally made by a high schooler.

There is a lesson to be learned here, folks. If you can tell what it is, I would be most appreciated.


What's in a Name?
=================

perihelion (peri·he·lion):
	the point in the path of a celestial body (as a planet)
	that is nearest to the sun — compare aphelion.
	:title:`Merriam-Webster.com. 2013.`

It's like Helion but *better.* Or close enough. Why was the original game called Helion? No other reason than it sounded like a cool, space-y word at the time. Astronomy rules.

Yes, there are other games also called "Perihelion". As they are not made by me, the reader is encouraged to confute their existence.


.. LINKS:
.. _CoffeeScript: http://coffeescript.org
.. _Rehab: https://github.com/Vizir/rehab
.. _Node.js: http://nodejs.org
.. _CoffeeLint: http://www.coffeelint.org
.. _UglifyJS2: http://lisperator.net/uglifyjs
.. _PIXI.js: http://www.pixijs.com/
.. _CreateJS: http://createjs.com